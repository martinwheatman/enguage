package org.enguage.sign.symbol.pronoun;

import org.enguage.sign.interpretant.Felicity;
import org.enguage.sign.symbol.config.Plural;
import org.enguage.sign.symbol.pattern.Pattern;
import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Audit;
import org.enguage.util.audit.Test;
import org.enguage.util.strings.Strings;

public class Pronoun {
	public  static final String NAME = "pronoun";
	public  static final int      ID = 198601099; // Strings.hash( NAME )
	private static final Audit audit = new Audit( NAME );
	
	// hard-coded three dimensions...
	public  static final    int SUBJECTIVE  = 0;
	public  static final    int  OBJECTIVE  = 1;
	public  static final    int POSSESSIVE  = 2;
	
	public  static final    int    SINGULAR = 0;
	public  static final    int      PLURAL = 1;
	
	public  static final String    singular = "singular";
	public  static final String      plural = "plural";
	
	public  static final String  subjective = "subjective";
	public  static final String   objective = "objective";
	public  static final String subjectives = "subjectives";
	public  static final String  objectives = "objectives";
	public  static final String  possessive = "possessive";
		
	private static int plurality( String name ) {
		return !possessive( name ) && !Plural.isSingular( name ) ? PLURAL : SINGULAR;
	}
	// ////////////////////////////////////////////////////////////////////////
	
	private static String[][][] pronouns = { // I, we, us
			{{"i",   "it",  "he",  "she", "he or she"  },   // singular subjective
			 {"we",  "they"                            }},  // plural
			{{"me",  "it",  "him", "her", "him or her" },   // singular objective
			 {"us",  "them"                            }},  // plural
			{{"my",  "its", "his", "her", "his or her" },   // singular possessive
			 {"our", "their"                           }} };// plural
	
	public  static String pronoun(int os, int ps, int mf) {
		if (os < SUBJECTIVE   || os > POSSESSIVE)   audit.FATAL( "pronoun - SUBJECTIVE fail" );
		if (ps < SINGULAR     || ps > PLURAL)       audit.FATAL( "pronoun - SNG/PLURAL fail" );
		if (mf < Gendered.MIN || mf > Gendered.MAX) audit.FATAL( "pronoun - GENDERED fail" );
		return pronouns[os][ps][mf];
	}
	private static void set(int os, int ps, int mf, String val) {
		pronouns [os][ps][mf] = val;
		if (ps == PLURAL) {
			if (os == SUBJECTIVE) {
				if (mf == Gendered.PERSONAL)
					Pattern.subjGroup( val );
				else if (mf == Gendered.NEUTRAL)
					Pattern.subjOther( val );
			} else if (os == OBJECTIVE) {
				if (mf == Gendered.PERSONAL)
					Pattern.objGroup( val );
				else if (mf == Gendered.NEUTRAL)
					Pattern.objOther( val );
			} else if (os == POSSESSIVE) {
				if (mf == Gendered.PERSONAL)
					Pattern.possGroup( val );
				else if (mf == Gendered.NEUTRAL)
					Pattern.possOther( val );
	}	}	}
	
	private static String[][][] values = { // initialise to names!
			{{"",   "",    "",   "",    ""  },   // singular subjective
			 {"",   "",    "",   "",    ""  }},  // plural
			{{"",   "",    "",   "",    ""  },   // singular objective
			 {"",   "",    "",   "",    ""  }},  // plural
			{{"",   "",    "",   "",    ""  },   // singular possessive
			 {"",   "",    "",   "",    ""  }} };// plural
	
	private static boolean possessivePn( String s ) {
		for (int k=Gendered.PERSONAL; k<=Gendered.UNGENDERED; k++)
			if (s.equals( pronouns[POSSESSIVE][SINGULAR][k])) return true;
		for (int k=Gendered.PERSONAL; k<=Gendered.NEUTRAL; k++) // only two plural pronouns
			if (s.equals( pronouns[POSSESSIVE][PLURAL][k])) return true;
		return false;
	}
	private static int type( String s ) {
		for (int i=SUBJECTIVE; i<=POSSESSIVE; i++) {
			for (int k=Gendered.PERSONAL; k<=Gendered.UNGENDERED; k++)
				if (s.equals( pronouns[i][SINGULAR][k])) return i;
			for (int k=Gendered.PERSONAL; k<=Gendered.NEUTRAL; k++) // only two plural pronouns
				if (s.equals( pronouns[i][PLURAL][k])) return i;
		}
		return -1;
	}
	private static int snglPlIs( String s ) {
		for (int i=SUBJECTIVE; i<=POSSESSIVE; i++) {
			for (int k=Gendered.PERSONAL; k<=Gendered.UNGENDERED; k++)
				if (s.equals( pronouns[i][SINGULAR][k])) return SINGULAR;
			for (int k=Gendered.PERSONAL; k<=Gendered.NEUTRAL; k++)
				if (s.equals( pronouns[i][PLURAL][k])) return PLURAL;
		}
		return -1;
	}
	private static int gend( String s ) {
		for (int i=SUBJECTIVE; i<=POSSESSIVE; i++) {
			for (int k=Gendered.PERSONAL; k<=Gendered.UNGENDERED; k++)
				if (s.equals( pronouns[i][SINGULAR][k])) return k;
			for (int k=Gendered.PERSONAL; k<=Gendered.NEUTRAL; k++)
				if (s.equals( pronouns[i][PLURAL][k])) return k;
		}
		return -1;
	}
	private static boolean isPronoun( String s ) { return type( s ) != -1;}

	// ------------------------------------------------------------------------
	private static int nameOs( String name ) {
		// TODO: equals and check pluraled
		return name.startsWith( subject ) ? SUBJECTIVE  :
		       name.equals(     object  ) ||
		       name.equals(     objects ) ? OBJECTIVE   :
		   	   name.equals(  possession ) ? POSSESSIVE  : -1; 
	}
	
	/////////////////// VARIABLE NAMES applicable to pronouns /////////////////
	///
	private static String subject = "subject";
	public  static  void   subjective( String nm ) { subject = nm; }
	
	private static String object = "this";
	public  static  void   objective( String nm ) { object = nm; }
	
	// These are the names of the parameters to use with plural
	// objective and subjective pronouns ("they" and "them")
	private static String subjects = "subjects";
	public  static  void   subjectives( String nm ) { subjects = nm; }
	public  static  String subjectives() { return subjects; }
	private static String objects = "these";
	public  static  void   objectives( String nm ) { objects = nm; }
	public  static  String objectives() { return objects; }
	
	// TODO: needs plural possessive, e.g. "s'"
	private static String possession = "'s";
	public  static  void   possession( String nm ) { possession = nm; }
	public  static  String possession() { return possession; }
	public  static boolean possessive( String word ) {
		return appended ?
				word.endsWith( possession )
				: word.startsWith( possession );
	}
	///
	//// VARIABLE NAMES applicable to pronouns ////////////////////////////////
	private static boolean appended = true;
	public  static  boolean appended() { return appended;}
	public  static  void    appended(boolean b) { appended = b;}
	
	// ////////////////////////////////////////////////////////////////////////
	// Interaction code...
	//
	private static Strings set( Strings sa ) {
		// the masculine personal objective pronoun is him =>
		//    set([ "objective", "singular", "masculine", "him" ]);
		Strings rc = Felicity.notOkay();
		if (sa.size() == 4) {
			int so = -1, mfn = -1, sp = -1;
			rc = Felicity.okay();
			while (sa.size() > 1) {
				String s = sa.remove( 0 );
				if (s.equals( singular ))
					sp = SINGULAR;
				else if (s.equals( plural ))
					sp = PLURAL;
				else if (s.equals( possessive ))
					so = POSSESSIVE;
				else if (s.equals( objective ))
					so = OBJECTIVE;
				else if (s.equals( subjective ))
					so = SUBJECTIVE;
				else if (s.equals( Gendered.personal ))
					mfn = Gendered.PERSONAL;
				else if (s.equals( Gendered.masculine ))
					mfn = Gendered.MASCULINE;
				else if (s.equals( Gendered.feminine ))
					mfn = Gendered.FEMININE;
				else if (s.equals( Gendered.neutral ))
					mfn = Gendered.NEUTRAL;
				else {
					rc = Felicity.notOkay();
					break;
			}	}
			if (rc.equals( Felicity.okay() ) && so != -1 && sp != -1 && mfn != -1)
				set( so, sp, mfn, sa.remove( 0 ));
		}
		return rc;
	}
	private static Strings name (String name, String value) {
		// e.g. name subjectives subjects
		Strings rc = Felicity.okay();
		if (name.equals( subjective ))
			subjective( value );
		
		else if (name.equals( subjectives ))
			subjectives( value );
		
		else if (name.equals( objective ))
			objective( value );
		
		else if (name.equals( objectives ))
			objectives( value );
		
		else if (name.equals( possessive ))
			possessive( value );
		
		else
			rc = Felicity.notOkay();

		return rc;
	}
	public  static Strings perform( Strings sa ) {
		// e.g. (pronoun) add masculine martin
		//      (pronoun) set OBJECTIVE PLURAL MASCULINE him
		//      (pronoun) name subjectives [subjects]
		audit.in( "interpret", ""+ sa );
		Strings rc = Felicity.notOkay();
		int sz = sa.size();
		if (sz > 0) {
			rc = Felicity.okay();
			String cmd = sa.remove( 0 );
			if (cmd.equals("set" ))
				rc = set( sa );
			else if (cmd.equals( "add" ))
				rc = Gendered.add( sa );
			else if (cmd.equals( "name" ))
				if (1==sz) // original size!
					rc = Felicity.notOkay(); // just cmd
				else
					name( sa.get( 0 ), sa.get( 1 ));
			else
				rc = Felicity.notOkay();
		}
		return audit.out( rc );
	}
	// ////////////////////////////////////////////////////////////////////////
	// get/set
	//
	private static void set( String name, String value ) {
		// set SUBJECT he     -> ignore
		//     SUBJECT martin => values[s][s][m]="martin", SUBJECT="martin"
		//     UNIT    cup    => unit="cup"
		int os = nameOs( name );
		if (!isPronoun( value ) && os != -1)
			values[os][plurality( value )][Gendered.valueMfn( value )] = new String( value );
	}
	private static String get( String name, String value ) {
		// N.B value is only used in get to deref pronoun table!
		//audit.in( "get", name + (isPronoun(value)?" (pronoun="+ value +")":""));
		int os = nameOs( name );
		return os != -1 && isPronoun( value ) ?
				values[os][plurality( value )][Gendered.valueMfn( value )]
				: value;
	}
	private static String update( String name, String value ) {
		String orig = value;
		set( name, value );
		String updated = get( name, value );
		return updated.equals( "" ) ? orig : updated;
	}
	private static void update( Attribute a ) {a.value( update( a.name(), a.value() ));}
	public  static Attributes update( Attributes as ) { for (Attribute a : as) update( a ); return as;}
	
	// ////////////////////////////////////////////////////////////////////////
	// -- test code
	//
	private static void possessiveOutbound( String value ) {
		String subj = get( subject, value );
		if (possessive( value ))
			Test.pass( "passes: "+ value +" => "+
					pronouns[POSSESSIVE][plurality( subj )][Gendered.valueMfn( subj )]);
		else
			audit.debug( "failed: "+ value +" != possessive"  );
	}
	private static void possessiveInbound( String pn ) {
		audit.in(  "possInternalising", pn );
		String ans = "internalising failed";
		// e.g. "his"->SUBJECT/"her"->SUBJECT/"its"->OBJECT/"their"
		if (possessivePn( pn ))
			Test.pass( "Passed: "+ pn +" => "+
					(ans = values[SUBJECTIVE][snglPlIs( pn )][gend( pn )]));
		else
			Test.pass( "Test fails: Pronoun type not possessive ("+
					type( pn ) +"!="+ POSSESSIVE +")" );
		audit.out( ans );
	}
	private static void testInterpret( String u ) { perform( new Strings( u ));}
	
	public  static void main( String[] args ) {
		
		testInterpret( "set singular subjective neutral they" );
		audit.debug( "pronoun: "+ pronouns[SUBJECTIVE][SINGULAR][Gendered.NEUTRAL]);
		
		testInterpret( "add masculine martin" );
		testInterpret( "add masculine jamie" );
		testInterpret( "add feminine  ruth" );
		testInterpret( "name subjective SUBJECT" );
		
		Audit.title( "Possession Test" );
		testInterpret( "add masculine martin" );
		set( subject, "martin" );
		
		Audit.subtl( "Outbound Test" );
		possessiveOutbound( "martin" );
		possessiveOutbound( "ruth's" );
		possessiveOutbound( "martin's" );
		
		Audit.subtl( "Inbound Test" );
		possessiveInbound( "him" );
		possessiveInbound( "her" );
		possessiveInbound( "his" );
		
		audit.debug( subject+"/he => "+ update( subject, "he" ));
		audit.debug( "FRED/he => "+ update( "FRED", "he" ));
		
		Attributes a = new Attributes();
		a.add( new Attribute( subject, "martin" ));
		a.add( new Attribute( subject, "he" ));
		audit.debug( "a="+ a );
		audit.debug( "a="+ update( a ));
		Test.passed();
}	}
