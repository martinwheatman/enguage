package org.enguage.sign.interpretant.intentions;

import org.enguage.sign.Config;
import org.enguage.sign.interpretant.Felicity;
import org.enguage.sign.symbol.Utterance;
import org.enguage.util.attr.Context;
import org.enguage.util.strings.Strings;
import org.enguage.util.strings.Terminator;

public class Reply {
	
	/** a Reply is a 'formatted' 'answer', with possibly a 'say' prefix
	 * [ prefix = "according to Douglas Adams"
	 *   answer = '42',
	 *   format = "the answer to life, the universe and everything is ..."
	 * ]
	 */
	
	public  static final String ANSWER_PH = "whatever"; // Placeholder

	private boolean repeated = false;
	public  void    repeated( boolean s ) {repeated = s;}
	public  boolean repeated()            {return repeated;}
	
	// This is used to control Intentions.mediate() loop
	private boolean done = false;
	public  Reply   doneIs( boolean b ) {done = b; return this;}
	public  boolean isDone() {return done;}
	
	private Felicity.Type  type = Felicity.Type.DNU;
	public  Felicity.Type  type() {return type;}
	public  Reply          type( Felicity.Type t ) {type = t; return this;}
	
	public boolean isFelicitous() {
		return  type == Felicity.Type.YES ||
				type == Felicity.Type.OK  ||
				type == Felicity.Type.CHS;
	}
	/* ------------------------------------------------------------------------
	 * Say list
	 */
	private static  Strings say = new Strings();
	public  static  Strings say() {return say;}
	public  static  void    say( Strings sa ) {
		if (sa == null)  // null to reset it!
			say = new Strings();
		else
			say.addAll( Terminator.addTerminator( sa ));
	}
	
	/* ------------------------------------------------------------------------
	 * Answer - a simple string "42" or list "coffee and biscuits"?
	 */
	private String noAnswer = "";	
	private String answer = noAnswer;	
	public  Reply  answer( String ans ) {answer = ans; return this;}
	public  String answer() {return answer;}
	
	/* ------------------------------------------------------------------------
	 * Format - the shape of the reply "x y Z" intention, e.g. x y 24
	 */
	private Strings format = new Strings();

	public  Strings format() {
		if (format.isEmpty())
			format = answer().isEmpty() ?
					Felicity.dnu() : new Strings( answer() ); 
			
		// if required put in answer (verbatim!)
		else if (format.contains( Strings.ELLIPSIS ))
			format.replace( Strings.ellipsis, answer() );
		
		else if (format.contains( Config.placeholder() ))
			format.replace( Config.placeholder(), answer() );
		
		// According to Wikipedia, ...  (set in config.xml)
		// If we've picked up a source, attribute it!
		//format = Attribution.attributeSource( format );
		
		return format;
	}
	public  Reply   format( Strings f ) {
		format = Context.deref( f );
		return this;
	}
	// ------------------------------------------------------
	
	private static  boolean verbatim = false; // set to true in handleDNU()
	private static  boolean isVerbatim() {return verbatim;}
	private static  void    verbatimIs( boolean val ) { verbatim = val; }

	/* previous() is used to retrieve the reply from the previous thought. It is
	 * used in implementing imagination.  If the imagination session goes ok,
	 * we need the reply from that session. Was implemented with the equiv 
	 * intention in previous C incarnation.
	 */
	private static  Strings previous = new Strings( "" );
	public  static  Strings previous( Strings rep ) { previous = rep; return previous; }
	public  static  Strings previous() {return previous;}
	
	/* ------------------------------------------------------------------------
	 * Common replies....
	 */
	public void toDnu( Strings thought ) {
		verbatimIs( true ); // repeat exactly on DNU
		// Construct the DNU format
		format( new Strings( Felicity.dnu() + ", ..." ));
		answer( thought.toString() );
		// must come after answer()
		type( Felicity.Type.SOZ );
		verbatimIs( false );
	}
	public void toIdk() {
		format( Felicity.dnk());
		type( Felicity.Type.DNK );
		answer( noAnswer ); // reset
	}
	
	/* ------------------------------------------------------------------------
	 * To strings....
	 */
	public Strings toStrings() {

		Strings reply = Utterance.externalise( format(), isVerbatim() );
		
		if (Utterance.understoodIs( type() != Felicity.Type.DNU )) {
			// used in disambiguation ordering :/
			if (!repeated())
				previous( reply );
			
		} else
			toDnu( Utterance.previous() );
		
		return reply;
	}
	
	// NB: no toString() as this has the side effect of cancelling 
	// attribution: take attribution back out to top level Enguage?
	// ...and replyToString()
	
	/* ------------------------------------------------------------------------
	 *  Intention
	 */
	public Reply reply( Strings values ) {
		// Accumulate reply - currently "say this"
		if (values.equals( Config.accumulateCmds() )) // say so -- possibly need new intention type?
			Reply.say( toStrings() );
		
		// propagate reply and return - is currently "say so"
		else if (values.equals( Config.propagateReplys() ))
			doneIs( true ); // just pass out this reply
			
		else {// reply "I don't understand" is like an exception?
			type( Felicity.typeFromStrings( values ));
			format( new Strings( values ));
			doneIs( type() != Felicity.Type.DNU );
		}
		return this;
}	}
