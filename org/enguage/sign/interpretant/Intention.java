package org.enguage.sign.interpretant;

import org.enguage.sign.Config;
import org.enguage.sign.interpretant.intentions.Commands;
import org.enguage.sign.interpretant.intentions.Engine;
import org.enguage.sign.interpretant.intentions.Reply;
import org.enguage.sign.object.sofa.Perform;
import org.enguage.sign.interpretant.intentions.Thought;
import org.enguage.sign.object.Variable;
import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Context;
import org.enguage.util.audit.Audit;
import org.enguage.util.strings.Strings;

public class Intention {
	
	private static final String       NAME = "intention";
	private static final Audit       audit = new Audit( NAME );
	
	private static final class Types {
		public  static final String      THINK = "t";
		public  static final String         DO = "d";
		public  static final String        RUN = "n";
		public  static final String      REPLY = "r";
	}
	private static final class Modifiers {
		public  static final String        POS = "+"; // "ok,    ..." & "yes, ..."
		public  static final String        NEG = "-"; // "sorry, ..." & "no,  ..."
		public  static final String       YEAH = "y"; // "yes,   ..."
		public  static final String        NAH = "n"; // "no,    ..."
		public  static final String        MEH = "";
	}
	
	public  static final String THEN_THINK = Types.THINK + Modifiers.POS; // t+="do i need a coffee"
	public  static final String ELSE_THINK = Types.THINK + Modifiers.NEG;
	public  static final String  YEH_THINK = Types.THINK + Modifiers.YEAH;
	public  static final String  NAH_THINK = Types.THINK + Modifiers.NAH;
	
	public  static final String    THEN_DO = Types.DO    + Modifiers.POS;
	public  static final String    ELSE_DO = Types.DO    + Modifiers.NEG;
	public  static final String     YEH_DO = Types.DO    + Modifiers.YEAH;
	public  static final String     NAH_DO = Types.DO    + Modifiers.NAH;

	public  static final String   THEN_RUN = Types.RUN   + Modifiers.POS;
	public  static final String   ELSE_RUN = Types.RUN   + Modifiers.NEG;
	public  static final String    YEH_RUN = Types.RUN   + Modifiers.YEAH;
	public  static final String    NAH_RUN = Types.RUN   + Modifiers.NAH;
	
	public  static final String THEN_REPLY = Types.REPLY + Modifiers.POS; // "r+="ok, this is a positive message"
	public  static final String ELSE_REPLY = Types.REPLY + Modifiers.NEG;
	public  static final String  YEH_REPLY = Types.REPLY + Modifiers.YEAH;
	public  static final String  NAH_REPLY = Types.REPLY + Modifiers.NAH;
	
	// 'finally' intentions are run irrespective of outcome
	public  static final String    FINALLY = "f";

	public  static final int UNDEFINED     = 255;

	public  static final int N_THINK       = 0x00; // 0000 0000 = 0
	public  static final int N_DO          = 0x01; // 0000 0001 = 1
	public  static final int N_RUN         = 0x02; // 0000 0010 = 2
	public  static final int N_REPLY       = 0x03; // 0000 0011 = 3
	public  static final int N_ALLOP       = 0x04; // 0000 0100 = 4
	private static final int N_AUTOP       = 0x05; // 0000 0101 = 5
	
	// intention modifiers
	public  static final int  N_MEH        = 0x00; // 0000 0000 = 0
	public  static final int  N_THEN       = 0x10; // 0001 0000 = 16
	public  static final int  N_ELSE       = 0x20; // 0010 0000 = 32
	public  static final int  N_YEH        = 0x30; // 0011 0000 = 48
	public  static final int  N_NAH        = 0x40; // 0100 0000 = 64
	
	public  static final int N_THEN_THINK  = N_THEN | N_THINK; // = 16
	public  static final int N_ELSE_THINK  = N_ELSE | N_THINK; // = 32
	public  static final int N_YEAH_THINK  = N_YEH  | N_THINK; // = 48
	public  static final int N_NADA_THINK  = N_NAH  | N_THINK; // = 64

	public  static final int N_THEN_DO     = N_THEN | N_DO;    // = 17
	public  static final int N_ELSE_DO     = N_ELSE | N_DO;    // = 33
	public  static final int N_YEAH_DO     = N_YEH  | N_DO   ; // = 49
	public  static final int N_NADA_DO     = N_NAH  | N_DO   ; // = 65

	public  static final int N_THEN_RUN    = N_THEN | N_RUN;   // = 18
	public  static final int N_ELSE_RUN    = N_ELSE | N_RUN;   // = 34
	public  static final int N_YEAH_RUN    = N_YEH  | N_RUN  ; // = 50
	public  static final int N_NADA_RUN    = N_NAH  | N_RUN  ; // = 66
	
	public  static final int N_THEN_REPLY  = N_THEN | N_REPLY; // = 19
	public  static final int N_ELSE_REPLY  = N_ELSE | N_REPLY; // = 35
	public  static final int N_YEAH_REPLY  = N_YEH  | N_REPLY; // = 51
	public  static final int N_NADA_REPLY  = N_NAH  | N_REPLY; // = 67
		
	public  static final int N_THEN_ALLOP  = N_THEN | N_ALLOP; // = 20
	public  static final int N_ELSE_ALLOP  = N_ELSE | N_ALLOP; // = 36
	public  static final int N_YEAH_ALLOP  = N_YEH  | N_ALLOP; // = 52
	public  static final int N_NADA_ALLOP  = N_NAH  | N_ALLOP; // = 68
//	
//	public  static final int N_THEN_AUTOP  = N_THEN | N_AUTOP; // = 21
//	public  static final int N_ELSE_AUTOP  = N_ELSE | N_AUTOP; // = 
//	public  static final int N_YEAH_AUTOP  = N_YEH  | N_AUTOP; // = 
//	public  static final int N_NADA_AUTOP  = N_NAH  | N_AUTOP; // = 
	
	public  static final int N_FINALLY     = 0xfe;             // = 254
	
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	// Conditional stuff - felicity!
	// need to expand these to add yes and no and ok and sorry explicitly.
	//
	private static Strings conditionalFormat = new Strings( "if:,", ':' );
	public  static void    conditionalFormat( Strings s ) {
		if (s.size() == 2 ) // check it's ok to use!
			conditionalFormat = s;
		else
			audit.FATAL( "trying to set a format not in the format 'x:y' (in config.xml?)" );
	}
	private static Strings conditionalFormat( String insert ) {
		Strings rc = new Strings();
		rc.add(conditionalFormat.get(0));
		rc.add( insert );
		rc.add(conditionalFormat.get(1));
		return rc;
	}
	
	private static String  soPrefix = "so";
	private static Strings soPrefix() {return conditionalFormat( soPrefix );}
	public  static void    soPrefix( String s ) {soPrefix = s;}
	
	private static String  negPrefix = "not";
	private static Strings negPrefix() {return conditionalFormat( negPrefix );}
	public  static void    negPrefix( String s ) {negPrefix = s;}
	
	private static String  yesPrefix = "yes";
	private static Strings yesPrefix() {return conditionalFormat( yesPrefix );}
	public  static void    yesPrefix( String s ) {yesPrefix = s;}
	
	private static String  noPrefix = "no";
	private static Strings noPrefix() {return conditionalFormat( noPrefix );}
	public  static void    noPrefix( String s ) {noPrefix = s;}
	
	public  static final int condType( int base,
			boolean isThen, boolean isElse,
			boolean isYes, boolean isNah ) {
		if (isThen) return base | N_THEN;
		if (isElse) return base | N_ELSE;
		if (isYes) return base | N_YEH;
		if (isNah) return base | N_NAH;
		return base;
	}
	private static int condTypeFromHookStr(
			String hook, boolean pos, boolean neg, boolean yeh, boolean nah )
	{
		if (      hook.equals(    Hook.DO_HOOK))
			return condType( N_DO,    pos, neg, yeh, nah );
		
		else  if (hook.equals(   Hook.RUN_HOOK ))
			return condType( N_RUN,   pos, neg, yeh, nah );
		
		else  if (hook.equals( Hook.REPLY_HOOK ))
			return condType( N_REPLY, pos, neg, yeh, nah );
			
		else  if (hook.equals( Hook.FNLLY_HOOK ))
			return N_FINALLY;
		
		return UNDEFINED;
	}
	private static String getCond( Strings sa ) {
		if (sa.begins( soPrefix() )) {
			sa.remove( 0, soPrefix().size()); // e.g. 'if', 'so', ','
			return Modifiers.POS;
		} else if (sa.begins( negPrefix() )) {
			sa.remove( 0, negPrefix().size()); // e.g.'if', 'so', ','
			return Modifiers.NEG;
		} else if (sa.begins( yesPrefix() )) {
			sa.remove( 0, yesPrefix().size()); // e.g.'if', 'so', ','
			return Modifiers.YEAH;
		} else if (sa.begins( noPrefix() )) {
			sa.remove( 0, noPrefix().size()); // e.g.'if', 'so', ','
			return Modifiers.NAH;
		}
		return Modifiers.MEH;
	}
	public static int extractType(Strings sa) {
		// Removes strings from sa!!!  E.g.:
		// [ "if", "so", ",", "think", "something" ] => 
		//   N_THEN_THINK + [ "think", "something" ]
		// [ "if", "not", ",", "say", "so" ] =>
		//   N_ELSE_REPLY + []
		int rc = UNDEFINED;
		String cond = getCond( sa );
		boolean neg = cond.equals( Modifiers.NEG );
		boolean pos = cond.equals( Modifiers.POS );
		boolean yes = cond.equals( Modifiers.YEAH );
		boolean  no = cond.equals( Modifiers.NAH );
		
		
		if (sa.equals( Config.propagateReplys()) ||
			sa.equals( Config.accumulateCmds()) )
		{
			// don't remove these 'specials', pass on & define in config.xml
			rc = condType( N_REPLY, pos, neg, yes, no );
			
		} else { // reply "xyz"
			int len = sa.size();
			String hook  = len>0?sa.get(0):"";
			String param = len>1?sa.get(1):"";
			
			if (Strings.isQuoted( param )) {
				// one should be "reply", "perform", etc.
				rc = condTypeFromHookStr( hook, pos, neg, yes, no );
				if (rc != UNDEFINED) {
					sa.remove(0); // ["perform" | "run" | "reply" | "finally" ]
					sa.set( 0, Strings.trim( sa.get(0), '"' ));
		}	}	}
		
		if (rc == UNDEFINED)
			rc = condType( N_THINK, pos, neg, yes, no );
		
		return rc;
	}
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////

	public Intention( int t, Strings vals ) {this( t, vals.toString());}
	public Intention( int t, String v ) {type=t; value=v; values=new Strings(v);}
	public Intention( Intention in, boolean temp, boolean spatial ) {
		this( in.type(), in.value() );
		temporalIs( temp );
		spatialIs( spatial );
	}
	
	private final int    type;
	public  final int    type() {return type;}
	
	// ***********************************************
	// ***********************************************
	private       String    value;
	private       Strings   values;
	public        String    value() {return value;}
	public        Intention value(String v) {
		value  = v;
		values = new Strings( v );
		return this;
	}
	public        Intention values(Strings vs) {
		values = vs;
		value  = vs.toString();
		return this;
	}
	// ***********************************************
	// ***********************************************

	private boolean   temporal = false;
	public  boolean   isTemporal() {return temporal;}
	public  Intention temporalIs( boolean b ) {temporal = b; return this;}

	private boolean   spatial = false;
	public  boolean   isSpatial() {return spatial;}
	public  Intention spatialIs( boolean s ) {spatial = s; return this;}

	// "ok, PERSON needs ..." => "ok martin needs a coffee"
	public  static Strings format( Strings values, String answer, boolean expand ) {
		return 	Variable.deref( // $BEVERAGE + _BEVERAGE -> ../coffee => coffee
					Context.deref( // X => "coffee", singular-x="80s" -> "80"
						new Strings( values )
								.replace( Strings.ellipsis, answer ),
						expand
				)	);
	}
	
	/*
	 * Perform works at the code level to obtain/set an answer.
	 * This was initially the object model, but now includes any code.
	 */
	public  void    andFinally() {
		// we don't want Finally to affect the reply, so use a fresh one
		Perform.command( new Reply(), values );
	}
	
	private boolean skip( Reply r ) {return type != N_FINALLY && r.isDone();}
	
	private Reply tryNoOnlyMediation( Reply previous ) {
		switch (type) {
			case N_NADA_THINK:
				if (previous.type() == Felicity.Type.NO)
					previous = Thought.think( values, previous.answer() );
				break;
	
			case N_NADA_DO:
				if (previous.type() == Felicity.Type.NO)
					Perform.command( previous, values );
				break;
	
			case N_NADA_RUN:
				if (previous.type() == Felicity.Type.NO)
					previous = Commands.run(
							format( values, previous.answer(), false ).toString(),
							previous.answer()
					);
				break;
	
			case N_NADA_REPLY:
				if (previous.type() == Felicity.Type.NO)
					previous.reply( values );
				break;
			
			case N_NADA_ALLOP:
				if (previous.type() == Felicity.Type.NO)
					previous = Engine.interp( this, previous );
				break;
	
			default:
				audit.FATAL("Found unknown intention type:"+ type);		
		}
		return previous;
	}

	private Reply infelicitousMediation( Reply previous ) {
		switch (type) {
			case N_ELSE_THINK:
				if (!previous.isFelicitous())
					previous = Thought.think( values, previous.answer() );
				break;
				
			case N_ELSE_DO:
				if (!previous.isFelicitous())
					Perform.command( previous, values );
				break;
				
			case N_ELSE_RUN:
				if (!previous.isFelicitous())
					previous = Commands.run(
							format( values, previous.answer(), false ).toString(),
							previous.answer()
					);
				break;
				
			case N_ELSE_REPLY:
				if (!previous.isFelicitous())
					previous.reply( values );
				break;
			
			case N_ELSE_ALLOP:
				if (!previous.isFelicitous())
					previous = Engine.interp( this, previous );
				break;
			
			default:
 				previous = tryNoOnlyMediation( previous );
		}
		return previous;
	}
	private Reply tryYesOnlyMediation( Reply previous ) {
		switch (type) {
			case N_YEAH_THINK:
				if (previous.type() == Felicity.Type.YES)
					previous = Thought.think( values, previous.answer() );
				break;
		
			case N_YEAH_DO:
				if (previous.type() == Felicity.Type.YES)
					Perform.command( previous, values );
				break;
		
			case N_YEAH_RUN:
				if (previous.type() == Felicity.Type.YES)
					previous = Commands.run(
							format( values, previous.answer(), false ).toString(),
							previous.answer()
					);
				break;
		
			case N_YEAH_REPLY:
				if (previous.type() == Felicity.Type.YES)
					previous.reply( values );
				break;
				
			case N_YEAH_ALLOP:
				if (previous.type() == Felicity.Type.YES)
					previous = Engine.interp( this, previous );
				break;
				
			default:
 				previous = infelicitousMediation( previous );
		}

		return previous;
	}
	private Reply conditionalMediation( Reply previous ) {
		switch (type) {
			case N_THEN_THINK:
 				if (previous.isFelicitous())
 					previous = Thought.think( values, previous.answer() );
				break;
				
 			case N_THEN_DO:
 				if (previous.isFelicitous())
 					Perform.command( previous, values );
 				break;
			
 			case N_THEN_RUN:
 				if (previous.isFelicitous())
	 				previous = Commands.run(
	 						format( values, previous.answer(), false ).toString(),
	 						previous.answer()
	 				);
 				break;
 				
 			case N_THEN_REPLY:
 				if (previous.isFelicitous())
 					previous.reply( values );
 				break;
 				
 			case N_THEN_ALLOP:
 				if (previous.isFelicitous())
	 				previous = Engine.interp( this, previous );
 				break;

 			default:
 				previous = tryYesOnlyMediation( previous );
		}
		return previous;
	}
	public Reply mediate( Reply previous ) {
		audit.in( "mediate", typeToString( type ) +"='"+ value +"' on "+previous.type()+" "+(skip( previous )?" >skipping<":"" ));
		switch (type) { // run whatever...
 			case N_THINK:
 				previous = Thought.think( values,  previous.answer() );
 				break;
 				
 			case N_DO:
 				Perform.command( previous, values );
 				break;
 				
 			case N_RUN:
 				previous = Commands.run(
						format( values, previous.answer(), false ).toString(),
						previous.answer()
				);
 				break;
 				
 			case N_REPLY: previous.reply( values );
 				break;
 				
 			case N_ALLOP: previous = Engine.interp( this, previous );
 				break;
 				
			default:
				previous = conditionalMediation( previous );
 		}
		audit.out( "felicity="+ previous.type() +", format="+previous.format() +", answer="+ previous.answer() );
		return previous;
	}
	
	/* ------------------------------------------------------------------------
	 * Printy bits
	 */
	public static String typeToString( int type ) {
		switch (type) {
			case N_REPLY      : return Types.REPLY;
			case N_THEN_REPLY : return THEN_REPLY;
			case N_ELSE_REPLY : return ELSE_REPLY;
			case N_YEAH_REPLY : return THEN_REPLY;
			case N_NADA_REPLY : return ELSE_REPLY;
			case N_THINK      : return Types.THINK;
			case N_THEN_THINK : return THEN_THINK;
			case N_ELSE_THINK : return ELSE_THINK;
			case N_YEAH_THINK : return THEN_THINK;
			case N_NADA_THINK : return ELSE_THINK;
			case N_DO         : return Types.DO;
			case N_THEN_DO    : return THEN_DO;
			case N_ELSE_DO    : return ELSE_DO;
			case N_YEAH_DO    : return THEN_DO;
			case N_NADA_DO    : return ELSE_DO;
			case N_RUN        : return Types.RUN;
			case N_THEN_RUN   : return THEN_RUN;
			case N_ELSE_RUN   : return ELSE_RUN;
			case N_YEAH_RUN   : return THEN_RUN;
			case N_NADA_RUN   : return ELSE_RUN;
			case N_ALLOP      : return Engine.NAME;
			case N_AUTOP      : return NAME;
			case N_FINALLY    : return FINALLY;
			default:
				audit.FATAL( "Intention: returning undefined for: "+ type );
				return "Aleady Exited on type " + type;
	}	}
	public String toString() {
		switch (type) {
			case N_THINK      : return value;
			case N_DO         : return "perform \""+ value +"\"";
			case N_RUN        : return "run \""+ value +"\"";
			case N_REPLY      : return "reply \""+ value +"\"";
			case N_THEN_THINK : return "if so, "+  value;
			case N_THEN_DO    : return "if so, perform \""+ value +"\"";
			case N_THEN_RUN   : return "if so, run \""+ value +"\"";
			case N_THEN_REPLY : return "if so, reply \""+ value +"\"";
			case N_YEAH_THINK : return "if yes, "+  value;
			case N_YEAH_DO    : return "if yes, perform \""+ value +"\"";
			case N_YEAH_RUN   : return "if yes, run \""+ value +"\"";
			case N_YEAH_REPLY : return "if yes, reply \""+ value +"\"";
			case N_ELSE_THINK : return "if not, "+ value;
			case N_ELSE_DO    : return "if not, perform \""+ value +"\"";
			case N_ELSE_RUN   : return "if not, run \""+ value +"\"";
			case N_ELSE_REPLY : return "if not, reply \""+ value +"\"";
			case N_NADA_THINK : return "if no, "+ value;
			case N_NADA_DO    : return "if no, perform \""+ value +"\"";
			case N_NADA_RUN   : return "if no, run \""+ value +"\"";
			case N_NADA_REPLY : return "if no, reply \""+ value +"\"";
			case N_FINALLY    : return "finally \""+ value +"\"";
			default : return Attribute.asString( typeToString( type ), value() );
	}	}
	
	// Various separators for sep(), below, 
	// and Intentions.toSpokenList()
	public static final String      AND_SEP = "; and, ";
	public static final String     THEN_SEP = "; then, ";
	public static final String     ELSE_SEP = "; otherwise, ";
	public static final String       IF_SEP = "if ";
	public static final String AND_THEN_SEP = "; and then, ";
	
	public String sep( boolean first ) {
		// a REPLY will force a break in the description.
		if (type == N_REPLY      ||
		    type == N_THEN_REPLY ||
		    type == N_ELSE_REPLY)
			return ELSE_SEP;
		else
			return first ? THEN_SEP : AND_THEN_SEP;
}	}
