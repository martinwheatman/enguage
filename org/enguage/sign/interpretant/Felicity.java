package org.enguage.sign.interpretant;

import java.util.ListIterator;
import java.util.Locale;

import org.enguage.util.audit.Trace;
import org.enguage.util.strings.Strings;

public class Felicity {
	private static final Trace trace = new Trace( "Felicity" );


	public enum Type {
		DNU( "DNU"   , false ), // DO NOT UNDERSTAND
		UDU( "UDU"   , false ), // user does not understand
		DNK( "DNK"   , false ), // NOT KNOWN -- init
		SOZ( "Sorry" , false ), // SORRY -- -ve
		NO(  "no"    , false ), // FALSE -- -ve
		OK(  "ok"    ,  true ), // TRUE  -- +ve identical to 'yes'
		YES( "yes"   ,  true ), // TRUE  -- +ve identical to 'yes'
		CHS( "chs"   ,  true ); // narrative verdict - meh!
		
		private Strings value;
		public  void    value( String s ) {value = new Strings( s.toLowerCase( Locale.getDefault() ));}
		public  Strings value() {return value;}
		
		public boolean stripable( Strings uttr ) { // "<type> ',' ..."
			return (   uttr.size() > value.size()
					&& uttr.begins( value )
					&& uttr.get( value.size() ).equals( "," ));
		}
		
		private final boolean felicitous;
		public  final boolean felicitous() {return felicitous;}
		
		private Type( String s, boolean f ) {
			value = new Strings( s );
			felicitous = f;
	}	}
	
	public static final Type typeFromStrings( Strings uttr ) {
//		     if (Type.YES.equals( uttr )) return Type.YES;
//		else if (Type.OK .equals( uttr )) return Type.OK;
//		else if (Type.SOZ.equals( uttr )) return Type.SOZ;
//		else if (Type.DNU.equals( uttr )) return Type.DNU; // sorry, i don't think...
//		else if (Type.UDU.equals( uttr )) return Type.UDU;
//		else if (Type.NO .equals( uttr )) return Type.NO;
//		else if (Type.DNK.equals( uttr )) return Type.DNK;
		     if (uttr.begins(     yes() )) return Type.YES;
		else if (uttr.begins(    okay() )) return Type.OK;
		else if (uttr.begins( notOkay() )) return Type.SOZ;
		else if (uttr.begins(     dnu() )) return Type.DNU; // sorry, i don't think...
		else if (uttr.begins(     udu() )) return Type.UDU;
		else if (uttr.begins(      no() )) return Type.NO;
		else if (uttr.begins(     dnk() )) return Type.DNK;
		else return Type.CHS;
	}
	public static final boolean isFelicitous( Strings uttr ) {
		return typeFromStrings( uttr ).felicitous();
	}
	
	public static final Strings strip( Strings s ) {
		trace.in( "strip", "s="+ s );
		Felicity.Type type = Felicity.typeFromStrings(s); 
		if (type.stripable( s )) {
			ListIterator<String> si = type.value().listIterator();
			while (si.hasNext()) {
				s.remove( 0 ); // remove nth value
				si.next();
			}
			s.remove( 0 ); // remove ','
		}
		return trace.out( s );
	}


	private static Strings build( Type t, String s ) {
		Strings rc = new Strings( t.value() );
		if (!s.equals( "" )) {
			rc.add( "," );
			for (String str : new Strings( s ))
				rc.add( str );
		}
		return rc;
	}
	private static Strings build( Type t, Strings s ) {
		Strings rc = new Strings( t.value() );
		rc.add( "," );
		rc.addAll( s );
		return rc;
	}

	public  static Strings okay() {return Type.OK.value();}
	public  static Strings okay( String s ) {return build( Type.OK, s );}

	public  static Strings notOkay() {return Type.SOZ.value();}
	public  static Strings notOkay( String s ) {return build( Type.SOZ, s );}
	
	public  static Strings dnk() {return Type.DNK.value();}
	public  static Strings dnk( String s ) {return build( Type.DNK, s );}
	
	public  static Strings no() {return Type.NO.value();}
	public  static Strings no(  String s ) {return build( Type.NO,  s );}
	
	public  static Strings yes() {return Type.YES.value();}
	public  static Strings yes( String s ) {return build( Type.YES, s );}
	
	public  static Strings dnu(){return Type.DNU.value();}
	public  static Strings dnu( String s ) {return build( Type.SOZ, build( Type.DNU, s ));}
	
	public  static Strings udu() {return Type.UDU.value();}
	public  static Strings udu( String s ) {return build( Type.UDU, s );}
}
