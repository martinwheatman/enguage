package org.enguage.sign.interpretant;

import java.util.ArrayList;
import java.util.Iterator;

import org.enguage.sign.interpretant.intentions.Reply;
import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Audit;
import org.enguage.util.strings.Strings;

public class Intentions extends ArrayList<Intention> {
	
	//private static final Audit audit = new Audit("Intentions"
	private static final long  serialVersionUID = 0L;

	public  Intentions append(        Intention in ){add( in ); return this;}
	public  Intentions insert( int i, Intention in ){add( i==-1 ? 0 : i, in ); return this;}

	public Intentions insert( Intention intn ) {
		int sz = size();
		if (sz == 0)
			insert( 0, intn );
		else {
			// reply always goes at the end
			Intention in = get( sz-1 );
			if (in.type() == Intention.N_REPLY)
				insert( sz-1, intn );
			else // end is not a reply, append list
				add( intn );
		}
		return this;
	}

	private String getSep( String sep, String line ) {
		if (line.startsWith( Intention.IF_SEP )
				&& (sep.equals( Intention.THEN_SEP ) ||
					sep.equals( Intention.AND_THEN_SEP )))
		{ // X, and, if so Y
			sep = Intention.AND_SEP;
		}
		return sep;
	}

	// ------------------------------------------------------------------------
	// Printing list in various formats...
	public String toXml() {
		StringBuilder intentions = new StringBuilder();
		Audit.incr();

		String indent = Audit.indent();
		for (Intention in : this)
			intentions.append(
					"\n"+ indent +
					Attribute.asString( 
							Intention.typeToString( in.type() ),
							in.value()
			)		);
		
		Audit.decr();
		return intentions.toString();
	}
	// ------------------------------------------------------------------------
	public String toStringIndented( boolean auditIntents ) {
		StringBuilder sb = new StringBuilder();
		int sz = size();
		if (sz == 1)
			sb.append( ", "+ get( 0 ));
		else if (sz > 1) {
			int line = 0;
			for (Intention in : this)
				sb.append(
						(line++ == 0 ? ":" : ";") + "\n"
						+ (auditIntents ? Audit.indent():"") +"    "+ in
				);
		}
		return sb.toString();
	}
	// ------------------------------------------------------------------------
	// simple printing of list of intentions, "what does this imply"
	// called from Spoken factory - perform().doList()
	public Strings toSpokenList() {
		Strings sb = new Strings();
		if (this.isEmpty()) {
			sb.append( "nothing" );
		} else {
			int line = 0;
			String sep = "";
			for (Intention in : this) {
				String str = in.toString();
				sep = getSep( sep, str );
				sb.append( sep );
				// "SOMETHING" -> "that something"
				for (String s : new Strings( str )) { // English-isms
					if (Strings.isUpperCase( s ) && !s.equals("A")
							&& !s.equals("I") && !s.equals(",")) // I is ok
						sb.append( "that" );
					sb.append( s.toLowerCase() );
				}

				sep = in.sep( line==0 );
				line = sep.equals( Intention.ELSE_SEP ) ? 0 : ++line;
		}	}
		return sb;
	}
	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	// to implies description... "what does X imply"
	// called from Repertoires.perform().Signs.listIntents()
	private Strings toSpoken( String s, Attributes matches ) {
		Strings strs = new Strings();
		if (s.equals( "..." )) // lets not put in Config.placeholder()!
			strs.append( "whatever-it-was" );
			
		else if (Strings.isUpperCase( s ) && !s.equals("A")
				&& !s.equals("I") && !s.equals(","))
		{ // I is ok
			String tmp = matches.deref( s );
			if (!tmp.equals( s ))
				strs.append( tmp );
			else
				strs.append( s.toLowerCase() );
			
		} else
			strs.append( s );

		return strs;
	}
	private Strings fromRemovedQuotes( Attributes matches, String str ) {
		Strings strs = new Strings();
		for (String s : new Strings( str )) {
			if (Strings.isQuoted( s )) {
				s = Strings.stripQuotes( s );
				for (String t : new Strings( s ))
					strs.addAll( toSpoken( t, matches ));
			} else
				strs.addAll( toSpoken( s, matches ));
		}
		return strs;
	}
	public Strings toSpokenList( Attributes matches ) {
		Strings strs = new Strings();
		if (this.isEmpty()) {
			strs.append( "nothing" );
		} else {
			int line = 0;
			String sep = "";
			for (Intention in : this) {
				String str = in.toString();
				
				sep = getSep( sep, str );
				strs.append( sep );
				
				strs.addAll( fromRemovedQuotes( matches, str ));
				
				sep = in.sep( line==0 );
				line = sep.equals( Intention.ELSE_SEP ) ? 0 : ++line;
		}	}
		return strs;
	}
	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	
	public Reply mediate() {
		// It's okay where there are no intentions (e.g. "i can say X")
		Reply r = new Reply().answer( Felicity.okay().toString() );
		r.type( Felicity.typeFromStrings( Felicity.okay() ));
		
		Iterator<Intention> ai = this.iterator();
		while (ai.hasNext()) {
			Intention in = ai.next();

			if (in.type() == Intention.N_FINALLY)
				in.andFinally();

			else if (!r.isDone()) // 'done' is maintained within r
				// think, do, say...
				// ...think (&run) will replace r
				r = in.mediate( r );
		}
		return r;
}	}
