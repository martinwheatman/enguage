package org.enguage.sign.object.sofa;

import org.enguage.repertoires.Repertoires;
import org.enguage.repertoires.concepts.Concept;
import org.enguage.repertoires.concepts.Similarity;
import org.enguage.sign.Assets;
import org.enguage.sign.Sign;
import org.enguage.sign.interpretant.Felicity;
import org.enguage.sign.interpretant.Intention;
import org.enguage.sign.interpretant.intentions.Reply;
import org.enguage.sign.object.Numeric;
import org.enguage.sign.object.Temporal;
import org.enguage.sign.object.Variable;
import org.enguage.sign.object.expr.Function;
import org.enguage.sign.object.list.Item;
import org.enguage.sign.object.list.Items;
import org.enguage.sign.object.list.Transitive;
import org.enguage.sign.symbol.config.Colloquial;
import org.enguage.sign.symbol.config.Plural;
import org.enguage.sign.symbol.pronoun.Pronoun;
import org.enguage.sign.symbol.when.Moment;
import org.enguage.sign.symbol.when.When;
import org.enguage.sign.symbol.where.Where;
import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Context;
import org.enguage.util.audit.Audit;
import org.enguage.util.html.Extract;
import org.enguage.util.html.Http;
import org.enguage.util.strings.Strings;
import org.enguage.util.wikipedia.InfoBox;
import org.enguage.util.wikipedia.Wikitext;

public class Perform {
	
	public  static final String  S_IGNORE  = "";
	
	private Perform() {}
	
	private static final Audit audit = new Audit( "Perform" );
	
	public  static Strings interpret( Strings a ) {
		if (a.isEmpty()) {
			audit.error("Perform interpret: not enough params: "+ a.toString());
		} else {
			/* Tags.matchValues() now produces:
			 * 		["a", "b", "c='d'", "e", "f='g'"]
			 * Perform.interpret() typically deals with:
			 * 		["entity", "get", "martin", "name"]
			 * 		["colloquial", "both", "'I have'", "'I've'"]
			 */
			String  type = a.remove( 0 );
			switch (Strings.hash( type )) {
				case Item       .ID: return        Item.perform( Attribute.expand23( a ));
				case Link       .ID: return        Link.perform(                     a  );
				case Http       .ID: return        Http.perform(                     a  );
				case Sign       .ID: return        Sign.perform( Attribute.expand(   a ));
				case Audit      .ID: return       Audit.perform( Attribute.expand(   a ));
				case Items      .ID: return       Items.perform(                     a  );
				case Value      .ID: return       Value.perform( Attribute.expand23( a ));
				case Where      .ID: return       Where.perform( Attribute.expand23( a ));
				case Plural     .ID: return      Plural.perform( Attribute.expand23( a ));
				case Assets     .ID: return      Assets.perform( Attribute.expand(   a ));
				case Entity     .ID: return      Entity.perform( Attribute.expand23( a ));
				case Concept    .ID: return     Concept.perform( Attribute.expand23( a ));
				case InfoBox    .ID: return     InfoBox.perform( Attribute.expand(   a ));
				case Pronoun    .ID: return     Pronoun.perform( Attribute.expand23( a ));
				case Context    .ID: return     Context.perform( Attribute.expand23( a ));
				case Numeric    .ID: return     Numeric.perform( Attribute.expand23( a ));
				case Overlay    .ID: return     Overlay.perform( Attribute.expand23( a ));
				case Extract    .ID: return     Extract.perform(                     a  );
				case Wikitext   .ID: return    Wikitext.perform( Attribute.expand(   a ));
				case Temporal   .ID: return    Temporal.perform( Attribute.expand23( a ));
				case Function   .ID: return    Function.perform( Attribute.expand23( a ));
				case Variable   .ID: return    Variable.perform( Attribute.expand23( a ));
				case Similarity .ID: return  Similarity.perform( Attribute.expand23( a ));
				case Colloquial .ID: return  Colloquial.perform(                     a  );
				case Transitive .ID: return  Transitive.perform( Attribute.expand23( a ));
				case Repertoires.ID: return Repertoires.perform(                     a  );
				default :
					audit.error( "Perform interpret: "+ type +".ID should be set to "+ Strings.hash( type ));
					return Felicity.notOkay();
		}	}
		return Felicity.notOkay();
	}
	private static final String ARGS = "args=";
	
	public  static void command( Reply r, Strings values) {command( r, values, false );}
	public  static void command( Reply r, Strings values, boolean ignore ) {
		audit.in( "command", "cmd='"+ values +"', ignore="+ (ignore?"yes":"no"));
		Strings cmd = Intention.format( values, r.answer(), true ); // DO expand, UNIT => unit='non-null value'
		
		// In the case of vocal perform, value="args='<commands>'" - expand!
		if (cmd.size()==1 &&
			cmd.get(0).length() > ARGS.length() &&
				cmd.get(0).substring(0,ARGS.length()).equals( ARGS ))
		{
			cmd=new Strings( new Attribute( cmd.get(0) ).value());
		}
		String method = cmd.get( 1 );
		
		Strings rawAnswer = Perform.interpret( cmd );
		
		if (!ignore) {
			if (rawAnswer.isEmpty() &&
				(method.equals( "get" ) ||
				 method.equals( "getAttrVal" )) )
			
				r.toIdk();
				
			else {  // Tidy this - answer to reply
				// Methods should return "OK", "Sorry" etc.
				// unless they return 5, 2 + 3 is 5
				// or just characters...
				// ...which just might be "a moment"
				String answer = rawAnswer.toString();
				if (Moment.valid( answer )) // 88888888198888 -> 7pm
					answer = new When( answer ).rep( Felicity.dnk() ).toString();
				Strings tmp = new Strings( answer );
				
				r.type( Felicity.typeFromStrings( tmp ));
				// so we've set the felicity, so remove it from answer
				r.format( new Strings( tmp ));
				tmp = Felicity.strip( tmp );
				r.answer( tmp.toString() );
				// answer, format::- yes, it is | 5, two plus three is 5.
		}	}
		audit.out( "felicity="+ r.type() +", format="+r.format() +", answer="+ r.answer() );
}	}
