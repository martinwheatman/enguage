package org.enguage.util.html;

import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.strings.Strings;
import org.enguage.util.sys.Fs;

public class Html {
	public  static final int      ID = 154478; // "html"
	
	public  enum Type {BEGIN,      // <...>
	                   STANDALONE, // <../>
	                   END}        // </..>
	
	private Type type = Type.BEGIN;
	public  Type type() {return type;}
	public  Html type( Type b ) {type = b; return this;}
	
	private String name = "";
	public  String name() {return name;}
	public  Html   name( String nm ) {name = nm; return this;}
	
	private Attributes attributes = new Attributes();
	public  Attributes attributes() {return attributes;}
	public  Html add( Attribute a ) {attributes.add( a ); return this;}
	
	public  boolean isEmpty() {return name.equals("");}
	
	public  static Strings initials( Strings args ) {
		// "table data" => "td"
		Strings rc = new Strings();
		StringBuilder word = new StringBuilder();
		for (String s : args )
			
			if (s.equals( "and" )) {
				rc.add( word.toString() );
				word = new StringBuilder();
				
			} else {
				if      (s.startsWith( "one" ))
					word.append( "1" );
				
				else if (s.startsWith( "two" ))
					word.append( "2" );
				
				else if (s.startsWith( "three" ))
					word.append( "3" );
				
				else
					word.append( s.substring( 0, 1 ));
			}
		rc.add( word.toString() );
		return rc;
	}

	public String toString() {
		return "<"
				+ (type==Type.END?"/":"")
				+ name
				+ attributes
				+ (type() == Type.STANDALONE?"/":"")
				+ ">";
	}
	public static void test( String[] args) {
		String fname = args.length == 0 ? "table.html" : args[ 0 ];
		if (args.length == 0) {
			String table = "junk in here<table class=\"infobox vcard\"><tbody>"
					+ "<tr><th colspan=\"2\">Elizabeth ii</th></tr>"
					+ "<tr><th>Born</th><td>Princess Elizabeth of York<br/>21 April, 1926<br/>Mayfair, London</td>"
					+ "</tr></tbody></table>";
			Fs.stringToFile( fname, table );
		}
		HtmlStream hs = new HtmlStream( fname );
		hs.prettyPrint();
}	}
