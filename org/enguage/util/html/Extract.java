package org.enguage.util.html;

import java.util.ListIterator;

import org.enguage.repertoires.Repertoires;
import org.enguage.sign.factory.Written;
import org.enguage.sign.interpretant.Felicity;
import org.enguage.sign.object.Variable;
import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Trace;
import org.enguage.util.strings.Strings;

public class Extract {
	private Extract() {} // Java needs a 'static' class modifier :^)
	
	public  static final int      ID = 70738021;
	private static final String NAME = "Extract";
	private static final Trace trace = new Trace( NAME );

	public  static final String EXTRACT_CONCEPT = "Html"+NAME;
	
	// This needs to be able to supply attributes like 
	// HtmlTable.doHtml() and Wikitext.doHtml().
	// Write them to a file for now.
	// Then retrieve the first line of born="value" 

	private static void expandParameters( ListIterator<String> ci ) {
		Attribute.expandNext( ci ); // get
		Attribute.expandNext( ci ); // query="abc"    => abc
		Attribute.expandNext( ci ); // location="xyz" => xyz
	}

	private static Topic topic;

	private static String source = "";
	public  static String source() {return source;}
	
	private static void ensignOptions( String websrc, String name, Attributes opts ) {
		// "wikipedia", "height", ["architectural='300m'"]
		// ...create signs...
		for( Attribute a : opts) {
			// e.g. "tip height", "Architectural height", ... 
			Repertoires.signs().insert(
				new Written(
					"On \""+ a.name() +" "+ name +"\", "
					+ "reply \"According to "+ websrc +", "
					+ "the "+ a.name() +" "+ name 
					+ " of "+ topic +" is "+ a.value() +"\""
				).toSign(),
				EXTRACT_CONCEPT
			);
		}
	}
	public static Attributes listHeaderGroup(
			Attributes as, String hname, String header )
	{
		// read through attributes, dipping in and remembering, header vals
		// .., header="height", "tip=300m", "topfl"=" 276m", header="other", ..
		Attributes rc = new Attributes();
		boolean list = false;
	
		ListIterator<Attribute> ni = as.listIterator();
		while (ni.hasNext()) {
			Attribute a = ni.next();
			
			if (a.name().equals( header ))
				list = a.value().equalsIgnoreCase( hname );
			
			else if (list)
				rc.add( a );
		}
		return rc;
	}

	public static Attributes listHdrValues( HtmlStream hs ) {
		trace.in( "listHdrValues", "<hs>" );
		// we've found a lone table header in the stream of th/td pairs.
		// read though th/td pairs until we find the next lone header.
		Attributes opts = new Attributes();
		Attributes as = hs.getTags(
				new Strings( "th td" ),
				HtmlStream.quantifier.ALL
		);
		ListIterator<Attribute> li = as.listIterator();
		while (li.hasNext()) {
			Attribute name = li.next();
			if (li.hasNext()) {
				Attribute value = li.next();
				if (value.name().equals( "th" ))
					break;
				else
					opts.add( new Attribute( name.value(), value.value() ));
		}	}
		trace.out( opts.toString() );
		return opts;
	}
	private static Strings getThTdValue( String fname, String name, String type ) {
		Strings rc;
		// 'infobox' value
		HtmlStream hs = new HtmlStream( fname );
		int n = hs.find( "th", new Strings( name )); // to Infobox
		
		if (n==-1)
			rc = Felicity.notOkay( "this "+ name +" tag was not found" );
		
		else {
			rc = hs.expectNext( "td",  type );
			
			if (Felicity.typeFromStrings(rc)==Felicity.Type.NO) {
				
				// On "no", we have found a header: list values...
				Attributes opts = listHdrValues( hs );
				String value = name;
				ensignOptions( Attribution.decodeSource( fname ), name, opts );
				
				// ...and prompt user to say one of these options:
				// [ "tip height", "or", "Architectural height", "or", ... 
				rc = Felicity.notOkay( "did you mean "+ opts.names().toString( " ", " "+value +" or ", " "+ value ));
				
			} else if (Felicity.typeFromStrings(rc)==Felicity.Type.SOZ)
				rc = Felicity.notOkay( "tag not found" );
		}
		return rc;
	}
	private static Strings getText( HtmlStream.quantifier quant, String fname, Strings args ) {
		Strings rc = new Strings();
		trace.in( "getText", "Fname="+ fname +", args=["+ Html.initials( args ) +"]");
		
		HtmlStream hs = new HtmlStream( fname );
		Attributes as = hs.getTags( Html.initials( args ), quant );
		
		if (!as.isEmpty()) {
			if (quant==HtmlStream.quantifier.ONE)
				rc = new Strings(as.get( 0 ).value());
			
			else {
				new Variable( "extract.attrs", as.toString( "\n" ));
				// try building : "extracted.attrs, 150 paragraphs and 7 "
				// + "header threes in file extracted.attrs" 
				rc = new Strings( "file is extract.attrs" );
		}	}
		return trace.out( rc );
	}

	private static Strings getSentence( Strings para ) {
		/* need to:
		 *     compress abbreviations. In Strings()? e.g. "U.S."
		 *     compress hyphenated words "mid-to-late".
		 *     remove names are replace with pronouns. Config!
		 */
		return Strings.copyUntil( para.listIterator(), "." );
	}
	
	public  static Strings perform( Strings args ) {
		Strings rc = Felicity.notOkay( "no values found" );
		trace.in( "perform", "args="+ args.toString( Strings.SQCSV ));

		if (!args.isEmpty()) {
			// expand variables nm="val" => val, and vars in progString
			expandParameters( args.listIterator() );
			args.prepend( Variable.stringExpand( args.remove( 0 )));
			
			// unwrap fname
			String fname = Strings.trim( args.remove( args.size()-1 ), '"' );
			Attribution.source( Attribution.decodeSource( fname )); // e.g. wikipedia
			topic = new Topic( fname );
			
			if (args.isEmpty())
				rc = Felicity.notOkay( "this is an empty query" );
			
			else {
				String cmd = args.remove( 0 );
				if (cmd.equals( "one" ))
					rc = getSentence( 
							getText( HtmlStream.quantifier.ONE, fname,  args )
						 );
					
				else if (cmd.equals( "all" )) 
					rc = getText( HtmlStream.quantifier.ALL, fname, args );
					
				else if (!args.isEmpty()) {
					String type = args.get( 0 ); 
					if (type.equals( "value" ) ||
						type.equals(  "date" ) ||
						type.equals(  "name" ) ||
						type.equals( "place" )    )
					{
						rc = getThTdValue( fname, cmd, type );
					}
		}	}	}
		return trace.out( rc );
}	}
