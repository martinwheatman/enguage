package org.enguage.util.html;

import org.enguage.sign.interpretant.Felicity;
import org.enguage.sign.symbol.when.Date;
import org.enguage.sign.symbol.where.Address;
import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Audit;
import org.enguage.util.audit.Indentation;
import org.enguage.util.audit.Trace;
import org.enguage.util.html.Html.Type;
import org.enguage.util.strings.Strings;
import org.enguage.util.token.Token;
import org.enguage.util.token.TokenStream;

public class HtmlStream {
	
	public  static final int          ID = 154478; // "html"
	private        final String filename;
	private static final Trace     trace = new Trace( "HtmlStream" );

	private              TokenStream  ts;

	public HtmlStream( String fname ) {
		filename = fname;
		ts = new TokenStream( fname );
	}
	
	public void reset() {
		// This should be derived from FileInputStream
		// which inherits reset()
		ts.close();
		ts = new TokenStream( filename );
	}
	
	private Attribute getAttribute( TokenStream ts ) {
		String  name = ts.getString();
		String value = "";
		if (name.matches( "[a-zA-Z-]+")) {
			if (ts.getString().equals("=")) { // '='
				value = ts.getString();
				value = Strings.trim( value, '\'' );
				value =	Strings.trim( value, '"' );
			} else
				value = name; // name => name'name'
		} else { // '/' or '>'
			ts.putBack();
			name = "";
		}
		return new Attribute( name, value );
	}
	public Html getHtml() {
		// double quotes: <name="value""> -- yes, dump this
		ts.dumpDoubleQuotes( true );
		Html h = new Html();
		while (ts.hasNext()) {
			if (ts.getString().equals( "<" )) {
				h.name( ts.getString() );
				if (h.name().equals("/")) {
					h.type( Html.Type.END );
					h.name( ts.getString() );
					ts.expectLiteral( ">" );
				} else {
					Attribute a = getAttribute( ts );
					while (!a.name().equals("")) { // isEmpty() !
						h.add( a );
						a = getAttribute( ts );
					}
					String word = ts.getString();
					if (word.equals( "/" )) {
						h.type( Type.STANDALONE );
						ts.getString();
				}	}
				break;
		}	}
		return h;
	}
	public Strings getText() {
		// double quotes: "\"""\"""'""'" -- don't dump
		ts.dumpDoubleQuotes( false );
		Strings text = new Strings();
		while (ts.hasNext()) {
			Token token = ts.getNext();
			if (token.string().equals("<")) {
				ts.putBack();
				break;
			}
			text.add( token.string() );
		}
		return text;
	}
	private Strings filterText() {
		Strings text = new Strings();
		Strings tmp  = getText();
		if (!tmp.isEmpty()) {
			if (tmp.get( 0 ).equals("&") && tmp.size() == 4) {
				String ch = tmp.get( 2 ); 
				if (ch.equals( "40" ))
					text.add( "(" );
				else if (ch.equals( "41" ))
					text.add( ")" );
				// else ignore
			} else
				text.addAll( tmp );
		}
		return text;
	}
	private Strings skipTag( Html html ) {
		int n = 1;
		String nm = html.name();
		while (n > 0) {
			html = getHtml(); // move to the next tag
			if (html.name().equals( nm ))
				n += html.type() == Type.END ? -1 : 1;
		}
		return filterText(); // return any trailing text!
	}
	
	public Strings getContent( String name ) {
		
		Strings text = getText();
		Html    html = getHtml();
		
		while (!html.name().equals( name ) ||
				html.type() != Type.END )
		{
			// in case text is broken into sections
			if (html.name().equals( "br" )) text.add( ";" );
			
			// skip style content
			if (html.name().equals( "style" ))
				getText(); // move to the next tag
			
			// skip hidden content
			else if (html.attributes().contains( "style", "display:none" ))
				text.addAll( skipTag( html ));
			
			else if (html.name().equals( "sup" ))
				skipTag( html );
			
			else
				text.addAll( filterText() );
			
			html = getHtml();
		}
		return text;
	}
	
	private Strings extractType( Strings rc, String type ) {
		if (type.equals( "date" ))
			return new Strings( Date.getDate(
					new Strings( ""+rc, ';' ),
					"sorry, I can't find a date" )
			);
			
		else if (type.equals( "place" ))
			return new Strings( Address.getAddress(
					new Strings( ""+rc, ';' ),
					"sorry, I can't find a place" )
			);
				
		else if (type.equals( "name" ))
			for (String s : new Strings( ""+rc, ';' )) {
				if (!Date.isDate( s ) &&
				    !Address.isAddress( s ))
				{
					return new Strings( s );
			}	}
		return rc;
	}

	public Strings expectNext( String name, String type ) {
		Strings rc;
		trace.in( "expectNext", "name="+ name +", type="+ type );
		
		// find the next begin
		Html h = getHtml();
		while (!h.isEmpty() &&
				(h.type() != Html.Type.BEGIN ))
			h = getHtml();
		
		if (h.isEmpty())
			rc = Felicity.notOkay( "end of file found" );
		else if (h.name().equals( name ))
			rc = extractType( getContent( name ), type );
		else
			rc = Felicity.no( "expected "+ name +", but got "+ h );
		
		return trace.out( rc );
	}

	public int size() {
		reset();
		int     n = 0;
		Html html = getHtml();
		while (!html.isEmpty()) {
			n++;
			html = getHtml();
		}
		return n;
	}
	
	public enum quantifier {ONE, ALL}
	
	private static final Strings emptyName = new Strings("");
	private int find( String tag, Strings name, boolean exact ) {
		reset();
		int  n = 0;
		Html h = getHtml();
		
		while (!h.isEmpty()) {
			
			if (   h.name().equals( tag )
				&& h.type() == Html.Type.BEGIN
				&& (name == null // name is optional!
					|| (!exact && getContent( h.name() ).contains( name ))
					|| ( exact && getContent( h.name() ).equals(   name ))
			   )   )
			{
				break;				
			}
			h = getHtml();
			n++;
		}
		return h.isEmpty() ? -1 : n;
	}
	public  int find( String tag, Strings name ) {
		if (name.equals( emptyName )) name = null;
		// see if there is an exact match first...
		int n = find( tag, name, true );
		if (-1 != n) return n;
		// otherwise see if there is a match...
		return find( tag, name, false );
	}
	
	public Attributes getTags( Strings names, quantifier quant ) {
		Attributes rc = new Attributes();
		trace.in( "getTags", "names="+ names +", quant="+ (quant==quantifier.ALL?"ALL":"ONE") );
		
		Html h = getHtml();
		while (!h.isEmpty()) {
			if (names.contains(    h.name() ) &&
				Html.Type.BEGIN == h.type()     )
			{
				Strings content = getContent( h.name() );
				if (!content.isEmpty()) { 
					rc.add(
						new Attribute( h.name(), ""+content )
					);
					if (quant==quantifier.ONE) break;
			}	}
			h = getHtml();
		}
		trace.out( rc.size() +" attributes found" );
		return rc;
	}
	public Html consumeTag( Html html ) {
		int n = 1;
		while (n>0 && !html.isEmpty()) {
			html = getHtml(); // this consumes text :)
			if (html.type() == Html.Type.END)
				n--;
			else if (html.type() == Html.Type.BEGIN)
				n++;
		}
		return html;
	}
	public void prettyPrint() {
		Indentation ind = new Indentation();
		StringBuilder sb = new StringBuilder();
		
		Strings strs = getText();
		if (!strs.isEmpty())
			sb.append( strs.toString() +"\n" );
		
		Html html = getHtml();
		while (!html.isEmpty()) {
			
			sb.append( ind.toString() + html.toString() );
			
			strs = getText();
			sb.append( strs.toString() +"\n" );
			
			if (html.type() == Type.BEGIN) ind.incr();
			else if (html.type() == Type.END) ind.decr();
			
			html = getHtml();
		}
		Audit.log( sb.toString() );
}	}
