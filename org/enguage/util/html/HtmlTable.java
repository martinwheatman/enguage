package org.enguage.util.html;

import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Trace;
import org.enguage.util.strings.Strings;

public class HtmlTable {
	
	public  static final int      ID = 0; // "html"
	private static final String NAME = "HtmlTable";
	private static final Trace trace = new Trace( NAME );

	private static final String TABLE = "table";
	private static final String TBODY = "tbody";
	private static final String    TR = "tr";
	
	public HtmlTable( String fname ) {
		attributes = doTBody( new HtmlStream( fname ));
	}
	private Attributes attributes = new Attributes();
	public  Attributes attributes() {return attributes;}
	
	private static String doCell( HtmlStream hs, String type ) {
		// "<td>Princess Elizabeth<br/>21 April, 1926<br/>Mayfair, London</td>
		//		=> ["Princess Elizabeth", "", ""]
		Strings s = hs.getText();
		boolean skipping = false;
		Html html = hs.getHtml();
		while ( !html.isEmpty() &&
				!(html.name().equalsIgnoreCase( type ) &&
				html.type()==Html.Type.END))
		{
			// don't put the HTML tag into the text
			// s.append( html.toString())
			// however, ...
			
			// deal with tables within <td/> - collapsible boxes
			if (html.name().equals( TABLE ))
				doTBody( hs );
			
			if (html.name().equals( "br" ))
				s.append( ";" );

			// Consume non-displayable text
			if (html.attributes().contains( "style", "display:none" ))
				html = hs.consumeTag( html );
			
			// skip script and style tags
			if (!html.name().equals( "script" ) &&
			    !html.name().equals( "style" ))
			{
				if (html.name().equals( "sup" ) &&
					html.type() == Html.Type.BEGIN)
					skipping = true;
				
				else if (html.name().equals( "sup" ) &&
					     html.type() == Html.Type.END)
					skipping = false;
				
				else {
					Strings text = hs.getText();
					if (!skipping)
						s.appendAll( text );
			}	}
			
			html = hs.getHtml();
		}
		return s.toString();
	}
	private static Attribute doTr( HtmlStream hs, boolean mergedTopRow ) {
		String name = "header"; // default name, if colspan=2
		String value = "";      // default value if no data
		
		Html html = hs.getHtml();
		while (!html.isEmpty()) {
			if (html.name().equalsIgnoreCase( "th" )) {
				
				if (html.attributes().contains("colspan","2") ||
						mergedTopRow)
					value = doCell( hs, "th" );
				else
					name = doCell( hs, "th" );
			
			} else if (html.name().equalsIgnoreCase( "td" ))
				value = doCell( hs, "td" );
			
			else if (html.name().equalsIgnoreCase( "tr" ) 
					&& html.type() == Html.Type.END)
				break;
			
			mergedTopRow = false;
			html = hs.getHtml();
		}
		
		return new Attribute( name, value );
	}
	// ====================================================
	private static Attributes doTBody( HtmlStream hs ) {
		Attributes attrs = new Attributes();
		Html html = hs.getHtml();
		trace.in( "doTBody", "next="+ html );
		while (!html.isEmpty() &&
				!(html.type() == Html.Type.END &&
				  html.name().equalsIgnoreCase( TBODY )))
		{	
			if (html.name().equalsIgnoreCase( TR )) {
				if (html.type() == Html.Type.END) {
					break;
				} else {
					Attribute attr = doTr(
						hs,
						html.attributes().contains( "class", "mergedtoprow" )
					);
					if (!attr.name().equals( "" ))
						attrs.add( attr );
			}	}
			
			html = hs.getHtml();
		}
		trace.out( attrs );
		return attrs;
	}
	private static Attributes doTable( HtmlStream hs ) {
		Attributes attrs = new Attributes();
		Html html = hs.getHtml();
		trace.in( "doTable", "next="+ html );
		while (!html.isEmpty() &&
				!(html.type() == Html.Type.END &&
				  html.name().equalsIgnoreCase( TABLE ))) {
			
			if (html.name().equalsIgnoreCase( TBODY ))
				attrs.addAll( doTBody( hs ));
			
			else if (html.name().equalsIgnoreCase( TABLE ))
				attrs.addAll( doTable( hs ));
			
			html = hs.getHtml();
		}
		trace.out( attrs );
		return attrs;
	}
	public static Attributes asAttributes( HtmlStream hs, int n ) {
		int i = -1;
		Attributes attrs = new Attributes();
		Html html = hs.getHtml();
		trace.in( "doHtml", "html="+ html );
		while (!html.isEmpty()) {
			
			if (html.name().equalsIgnoreCase( TABLE ) &&
				(n >= 0 &&  ++i == n))
			{
				attrs.addAll( doTable( hs ));
			}
			html = hs.getHtml();
		}
		trace.out(attrs);
		return attrs;
	}
	public static Attributes asAttributes( HtmlStream hs, String id ) {
		Attributes attrs = new Attributes();
		Html html = hs.getHtml();
		trace.in( "doHtml", "html="+ html );
		while (!html.isEmpty()) {
			
			if (html.name().equalsIgnoreCase( TABLE ) &&
				(id.equals( "" ) ||
						html.attributes().contains("class", id )))
			{
				attrs.addAll( doTable( hs ));
			}
			html = hs.getHtml();
		}
		trace.out(attrs);
		return attrs;
}	}
