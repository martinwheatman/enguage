package org.enguage.util.html;

import org.enguage.sign.interpretant.Felicity;
import org.enguage.util.strings.Strings;

public class Attribution {
	
	private Attribution() {}
	
	// Set in Config.java/config.xml
	private static Strings attributing = new Strings( "according to X," ).reverse();
	public  static Strings attributing() {return attributing;}
	public  static Strings attributing( String a ) {
		attributing = new Strings( a ).reverse();
		return attributing;
	}

	private static String source = "";
	public  static String source() {return source;}
	public  static void   source( String s ) {source = s;}
	
	public  static String decodeSource( String fname ) {
		// e.g. "./selftest/wikipedia/topic.html" => "wikipedia"
		String[] source = fname.split( "/" );
		// can't be certain of the length of the path...
		// ...so take if from the end!
		return source[ source.length - 2 ];
	}

	// this is only when replying to the user...
	public static Strings attributeSource( Strings reply ) {
		if (!source().equals( "" ) ) {
			// only attribute successful replies...
			if (!(reply.begins( Felicity.Type.SOZ.value() ) &&
			      reply.get( Felicity.Type.SOZ.value().size() ).equals( "," )))
			{
				if (reply.begins( Felicity.Type.OK.value() ) &&
					reply.get( Felicity.Type.OK.value().size() ).equals( "," ))
				{
					reply.remove( 0, 1 + Felicity.Type.OK.value().size() );
				}
				for (String s : attributing)
					reply.add( 0, s.equals( "X" ) ? source() : s );
			}
			// ...and finally, we want to scrub a source whether or not it was used!
			source( "" );
		}
		return reply;
}	}
