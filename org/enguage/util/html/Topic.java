package org.enguage.util.html;

public class Topic {
	
	private String name; // e.g. "Queen Elizabeth The Second"
	
	public  Topic( String fname ) {
		// fname = "./selftest/wikipedia/Queen_Elizabeth_The_Second.html"
		String[] source = fname.split( "\\." );
		String[]   path = source[ source.length - 2 ].split( "/" );
		name =  path[ path.length - 1 ].replace( "_", " " );
	}
	
	public  String toString() {return name;}
}
