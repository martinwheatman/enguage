package org.enguage.util.html;

import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Trace;
import org.enguage.util.strings.Strings;

public class HtmlText {

	private static final String NAME = "HtmlText";
	private static final Trace trace = new Trace( NAME );
	
	/* Text   : is an html table with the class="infobox vcard"
	 *          it is a 2 column table of th and td tags.
	 *          This class reads and presents it as an Attributes list.
	 *          E.g.:  Section title:     paragraphs
	 *          +-----------------------+----------------------------+
	 *          |                       | <p>Barack Hussein Obama... |
	 *          +-----------------------+----------------------------+
	 *          |                       | <p>                        |
	 *          +-----------------------+----------------------------+
	 *          | <h3> Early Life </h3> | <p>                        |
	 *          +-----------------------+----------------------------+
	 *          |                       | <p>...                            |
	 *          
	 * For testing, see:- get_the-page_from-.entry
	 */
	

	private String attrName = "intro";
	
	private Attributes doParagraph( HtmlStream hs ) {
		Attributes ats    = new Attributes();
		trace.in( "doParagraph", "" );
		
		Strings value     = hs.getText();
		boolean appending = true;
		
		Html html = hs.getHtml();
		while (!html.isEmpty() &&
				!(html.type() == Html.Type.END &&
				  html.name().equalsIgnoreCase("p")))
		{
			if (appending)
				value.addAll( hs.getText());
			
			html = hs.getHtml();
			if (html.name().equals( "sup" ))
				appending = html.type() == Html.Type.END;
		}
		if (!value.isEmpty())
			ats.add( new Attribute( attrName, value.toString() ));
		
		trace.out( ats );
		return ats;
	}
	private String doHeader( HtmlStream hs, String tagName ) {
		trace.in( "doHeader", "" );
		Strings strs = hs.getText();
		Html html = hs.getHtml();
		while (!html.isEmpty()  &&
				!(html.type() == Html.Type.END &&
				  html.name().equals(tagName)))
		{
			strs.addAll( hs.getText() );
			html = hs.getHtml();
		}
		return trace.out( strs.toString() );
	}
	public Attributes doHtml( HtmlStream hs ) {
		Attributes as = new Attributes();
		Html html = hs.getHtml();
		trace.in( "doHtml", "html="+ html );
		
		boolean parasfound = false;  // 'Contents' header is before paras
		
		while (!html.isEmpty()  &&
				!(html.type() == Html.Type.END &&
				  html.name().equalsIgnoreCase("html")))
		{
			if (parasfound &&
				(html.name().equalsIgnoreCase( "h2" ) ||
				 html.name().equalsIgnoreCase( "h3" ) ||
				 html.name().equalsIgnoreCase( "h4" )   ) &&
				 html.type() != Html.Type.END)
			{
				attrName = doHeader( hs, html.name() );
				
			} else if (html.name().equalsIgnoreCase(  "p" ) &&
					   html.type() != Html.Type.END)
			{
				parasfound = true;
				as.addAll( doParagraph( hs ));
			} else
				hs.getText();
			
			html = hs.getHtml();
		}
		trace.out( as );
		return as;
	}
}
