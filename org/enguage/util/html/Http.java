package org.enguage.util.html;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.ListIterator;

import org.enguage.sign.interpretant.Felicity;
import org.enguage.util.attr.Attribute;
import org.enguage.util.audit.Audit;
import org.enguage.util.audit.Trace;
import org.enguage.util.strings.Strings;
import org.enguage.util.sys.Fs;

public class Http implements AutoCloseable {

	public  static final int      ID = 154664;
	private static final String NAME = "Http";
	private static final Audit audit = new Audit(NAME);
	private static final Trace trace = new Trace(NAME);
	
	private HttpURLConnection connection;
	private HttpURLConnection connection() {return connection;}

	public Http( String url ) {
		try {
			// Create a URL object with the target endpoint
			URL u = new URL( url );
	
			// Open a connection to the URL
			connection = (HttpURLConnection) u.openConnection();
			connection.setRequestMethod("GET");
			
		} catch (IOException e) {
			e.printStackTrace();
	}	}
	
	public void close() {
		connection().disconnect();
	}
	// ************************************************************************
	
	public int responseCode() throws IOException {
		return connection().getResponseCode();
	}
	public String response() {
		StringBuilder response = new StringBuilder();
		// Read the response
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader( connection.getInputStream() )))
		{
			String line;
			while (null != (line = reader.readLine()))
				response.append( line+"\n" );
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response.toString();
	}
	
	// =====================================
	private static final String PROTOCOL = "https://";
	private static HashMap<String,String> sourceLoc = new HashMap<>();
	public  static void                   url( String urls ) {
		// "loca:urla.co.uk/query?x=;locnB:urlb.com/w?x=;"
		for (String spec : urls.split( ";" ))
			if (!spec.equals( "" )) {
				String[] config = spec.split( ":" );
				if (config.length == 2 ) // [ "locn", "url.com/query?x=" ]
					sourceLoc.put( config[ 0 ], PROTOCOL+config[ 1 ]);
			}
	}
	
	// =====================================
	private static Strings doGet( Strings cmds, String location, String url ) {
		Strings rc;
		// "nelson mandela" => "Nelson_Mandela"
		String title = Strings.stripQuotes(
					cmds.normalise() // => ["nelson", "mandela"]
					.capitalise()    // => ["Nelson", "Mandela"]
					.toString()      // => "Nelson_Mandela"
				).replace( " ", "_" );
		
		String cacheName = Fs.root()+ location +File.separator+ title +".html";
		
		// see if we've already got it...
		if (new File( cacheName ).exists()) {
			rc = Felicity.okay( "\""+ cacheName +"\"" );
			audit.debug( "Found: "+ rc );

		} else // otherwise go get it from the 'net
			try (Http http = new Http( url+title )) {
				
				audit.debug( "Downloading to: "+ cacheName );
				if (http.responseCode() == 200) {
					Fs.stringToFile(
							cacheName,
							http.response()
					);
					rc = Felicity.okay( "\""+ cacheName +"\"" );
					
				} else
					rc = Felicity.notOkay( "the response was "+http.responseCode());
				
			} catch (IOException e) {
				rc = Felicity.notOkay( "an exception has been generated: "+ e );
				e.printStackTrace();
			}
		return rc;
	}
	private static void expandParameters( ListIterator<String> ci ) {
		Attribute.skipNext(   ci ); // get
		Attribute.expandNext( ci ); // query="abc"    => abc
		Attribute.skipNext(   ci ); // from
		Attribute.expandNext( ci ); // location="xyz" => xyz
	}
	public static Strings perform( Strings cmds ) {
		// e.g. http get X from Y
		trace.in( "perform", "cmds="+ cmds.toString(Strings.DQCSV) );
		expandParameters( cmds.listIterator());
		
		Strings rc = Felicity.notOkay();
		String cmd = cmds.remove(0);
		
		cmds.remove(cmds.size()-2); // remove 'from'
		String location = cmds.remove( cmds.size()-1 );
		
		String url = sourceLoc.get( location );
		if (null == url)
			rc = Felicity.notOkay( "you're using an unknown location "+ location );
		
		else if (cmd.equals( "get" ))
			rc = doGet( cmds, location, url );
		
		trace.out( rc );
		return rc;
}	}
