package org.enguage.util.audit;

public class Trace extends Audit {
	
	public  Trace( String name ) {super(name);}
	
	public void   IN( String fn )              {super.IN( fn       );}
	public void   in( String fn )              {super.in( fn       );}
	public void   IN( String fn, String info ) {super.IN( fn, info );}
	public void   in( String fn, String info ) {super.in( fn, info );}
	public void   OUT()                        {         super.OUT();}
	public void   out()                        {         super.out();}
}
