package org.enguage.util.audit;

public class Test {
	
	private Test(){}
	
	private static int  numberIgnored = 0;
	public  static void ignore() {numberIgnored++;}
	public  static void ignore(String s) {Audit.log( s ); ignore();}
	
	private static int  numberOfTests = 0;
	public  static int  numberOfTests() {return numberOfTests;}
	public  static void pass() {numberOfTests++;}
	public  static void pass( String msg ) {Audit.log( msg ); pass();}
	
	public  static void passed() {
		Audit.log( "+++ "
			 + (numberIgnored==0?"ALL ":"")
			 + numberOfTests
			 + " tests "
			 + (numberIgnored==0 ?
			 	"passed":
				"PASSED ("
				+ numberIgnored
				+ " IGNORED)"
			   )
			 + " in "
			 + Audit.interval()
			 + "ms +++"
		);
}	}
