package org.enguage.util.wikipedia;

import java.nio.charset.StandardCharsets;
import java.util.ListIterator;

import org.enguage.repertoires.Repertoires;
import org.enguage.sign.factory.Written;
import org.enguage.sign.interpretant.Felicity;
import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Audit;
import org.enguage.util.audit.Trace;
import org.enguage.util.html.Attribution;
import org.enguage.util.html.Extract;
import org.enguage.util.html.HtmlStream;
import org.enguage.util.html.HtmlTable;
import org.enguage.util.html.Topic;
import org.enguage.util.strings.Strings;
import org.enguage.util.token.TokenStream;

public class InfoBox {
	
	public  static final int      ID = 113445711;
	private static final String NAME = "Infobox";
	private static final Trace trace = new Trace( NAME );
	
	private static final String INFO_BOX_CONCEPT = NAME+"Gen";
	public  static final String           HEADER = "header";

	/* Infobox: is a Wikipedia table with the class="infobox vcard"
	 *      it is a 2 column table of th and td tags. E.g.:
	 *      +-----------------------+----------------------------+
	 *      | Reign                 | <1st date>  --  <2nd date> |
	 *      +-----------------------+----------------------------+
	 *      | Born                  | Princess Elizabeth of York |
	 *      |                       | 21 April 1926              |
	 *      |                       | Mayfair London England     |
	 *      +-----------------------+----------------------------+
	 *      |                      Height                        |
	 *      +-----------------------+----------------------------+
	 *      | Tip                   | 330m                       |
	 *      +-----------------------+----------------------------+
	 *      | Top Floor             | 276m                       |
	 *      +-----------------------+----------------------------+
	 */
	
	private static final class Commands {
		public  static final String      GET =        "get";
		public  static final String LIST_VAL = "listValues";
		public  static final String LIST_HDR = "listHeader";
		public  static final String    GROUP =      "group";
	}
	
	private final String     fname;
	private final Attributes attrs;
	private final Topic      topic; // e.g. "Queen Elizabeth The Second"
	
	private final String     source;
	public        String     source() {return source;}
	
	private InfoBox( String fn ) {
		fname  = Strings.trim( fn, '"' );
		attrs  = HtmlTable.asAttributes(
						new HtmlStream( fname ),
						"infobox"
				);
		topic  = new Topic( fname );
		source = Attribution.decodeSource( fname );
	}
	
	private Strings doGet( Strings args ) {
		// args=[ "born date", "<filename>" ]
		Strings query = new Strings( args.get( 0 ));

		Strings ss = new Strings();
		ss.add( "name=\""+query.get(0)+"\"" );
		ss.add( "type=\""+getType( query )+"\"" );
		ss.add( "\""+fname+"\"" );
		
		return Extract.perform( ss );
	}
	private static String removeJunkBeforeColon( String name ) {
		Strings tmp = new Strings();
		try (TokenStream ts = new TokenStream( name.getBytes(StandardCharsets.UTF_8) ))
		{
			while (ts.hasNext()) {
				String str = ts.getString(); 
				if (str.equals( ";" ))
					tmp = new Strings();
				else
					tmp.append( (tmp.isEmpty() ? "" :" ") + str);
			}
		} catch (Exception x) {
			Audit.log( "ex:"+ x );
		}
		return tmp.toString();
	}
	private Strings doListVal() {
		Strings names = attrs.names();
		ListIterator<String> ni = names.listIterator();
		while (ni.hasNext())
			if (ni.next().equals( HEADER ))
				ni.remove();
		
		return new Strings( names.toString( Strings.DQCSV ));
	}
	private Strings doListHdr( Strings args ) {
		trace.in( "doListHdr", "args=["+ args +"]" );
		Strings         rc = new Strings();
		String      header = args.remove( 0 );
		Attributes headers =
				Extract.listHeaderGroup( attrs, header, HEADER ); // fr. Extract
		
		if (headers.isEmpty())
			rc = Felicity.notOkay( "the "+ header +" is not a header" );
		
		else if (headers.size() == 1)
			Felicity.okay(
					" the "
					+ headers.get( 0 ).name()
					+ " "
					+ header
					+ " of "
					+ topic
					+ " is "
					+ headers.get( 0 ).value()
			);
		else {
			ListIterator<Attribute> ai = headers.listIterator();
			while (ai.hasNext()) {
				Attribute adjective = ai.next();
				String name = removeJunkBeforeColon( adjective.name() );
				
				// e.g. "tip height", "Architectural height", ... 
				// see "according_to" dictionary entry
				rc.append( name +" "+ header );
				Repertoires.signs().insert(
					new Written( 
						"On \""+ name +" "+ header +"\""
						+ ", according to "+ source +" "
						+ "what is the "+ name +" of "+ header 
						+" from "+ topic
					).toSign(),
					INFO_BOX_CONCEPT
				);
			}
			
			// [ "tip height", "or", "Architectural height", "or", ... 
			rc = Felicity.notOkay( "is that "+ rc.toString( "", " or ", "" ));
		}
		return trace.out( rc );
	}
	private Strings doGroup( Strings args ) {

		String    group = args.remove(0);
		String attrName = args.remove(0);
		
		Strings rc = Felicity.notOkay( "attribute "+ attrName +" not found in "+ group );
		
		boolean checkMe = false;
		ListIterator<Attribute> ai = attrs.listIterator();
		while (ai.hasNext()) {
			Attribute attr = ai.next();
			
			if (attr.name().equals( HEADER ))
				checkMe = attr.value().equalsIgnoreCase(group);
			
			else if (checkMe && attr.name().contains( attrName ))
				rc = new Strings( attr.value());
		}
		return rc;
	}
	private static String getType( Strings name ) {
		// type = [value|date|name|address]
		String  type = name.get( name.size()-1 );
		if (type.equals( "date") ||
			type.equals("place") ||
			type.equals( "name") ||
			type.equals("value")   )
			// args is not the attribute name pattern
			type = name.remove( name.size()-1 );
		else
			type = "name";
		return type;
	}
	public  static Strings perform( Strings args ) {
		Strings rc    = Felicity.dnu( "infobox "+ args );
		trace.in( "perform", "args=\""+ args.toString( Strings.SQCSV ) +"\"");
		
		// args = [ '<cmd>', ... , '"<page>"' ]
		String  cmd    = args.remove( 0 );
		
		String lastArg = args.remove( args.size()-1 );
		InfoBox ib     = new InfoBox( lastArg );
		Attribution.source( ib.source() ); // e.g. wikipedia
		
		if      (cmd.equals( Commands.GET      ))
			// get VALUE PAGE
			rc = ib.doGet( args );
		
		else if (cmd.equals( Commands.GROUP    ))
			// group GROUP NAME PAGE
			rc = ib.doGroup( args );
			
		else if (cmd.equals( Commands.LIST_HDR ))
			// listHeader PAGE
			rc = ib.doListHdr( args );
			
		else if (cmd.equals( Commands.LIST_VAL ))
			// listValues PAGE
			rc = ib.doListVal();
		
		trace.out( rc );
		return rc;
}	}
