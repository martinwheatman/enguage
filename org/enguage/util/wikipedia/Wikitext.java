package org.enguage.util.wikipedia;

import org.enguage.sign.interpretant.Felicity;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Trace;
import org.enguage.util.html.Attribution;
import org.enguage.util.html.HtmlStream;
import org.enguage.util.html.HtmlText;
import org.enguage.util.strings.Strings;

public class Wikitext extends HtmlText {
	
	public  static final int      ID = 277584377;
	private static final String NAME = "Wikitext";
	private static final Trace trace = new Trace( NAME );
	
	private final String fname;
	private final Attributes attrs;
	
	private final String source;
	public  String source() {return source;}
	
	private Wikitext( String fn ) {
		fname  = fn;
		attrs  = doHtml( new HtmlStream( fname ));
		source = Attribution.decodeSource( fname );
	}
	
	public static Strings perform( Strings args ) {
		// args = [ '<cmd>', ... , '"<page>"' ]
		trace.in( "perform", "args=["+ args.toString( Strings.SQCSV )+"]");
		
		Strings    rc = Felicity.dnu( "wikitext perform "+ args );
		String    cmd = args.remove( 0 );
		String  fname = Strings.trim( args.remove( args.size()-1 ), '"' );
		
		Wikitext text = new Wikitext( fname );
		
		if (Disambiguation.isaDisabiguationPage( text.attrs ))
			rc = Disambiguation.disambiguate( cmd, args, fname );

		else {
			Attribution.source( text.source() ); // e.g. wikipedia
			if (cmd.equals( "get" )) {
				rc = new Strings( text.attrs.value( "intro" ));
				/* need to:
				 *     compress abbreviations. In Strings()? e.g. "U.S."
				 *     compress hyphenated words "mid-to-late".
				 *     remove names are replace with pronouns. Config!
				 */
				rc = Strings.copyUntil( rc.listIterator(), "." );
		}	}
		
		trace.out( rc );
		return rc;
}	}
