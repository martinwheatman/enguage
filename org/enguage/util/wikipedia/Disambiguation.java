package org.enguage.util.wikipedia;

import org.enguage.repertoires.Repertoires;
import org.enguage.sign.Config;
import org.enguage.sign.factory.Written;
import org.enguage.sign.interpretant.Felicity;
import org.enguage.util.attr.Attribute;
import org.enguage.util.attr.Attributes;
import org.enguage.util.audit.Audit;
import org.enguage.util.html.Html;
import org.enguage.util.html.HtmlStream;
import org.enguage.util.html.Topic;
import org.enguage.util.strings.Strings;

public class Disambiguation {
	
	public  static final int      ID = 113445711;
	private static final String NAME = "Disambiguation";
	private static final Audit audit = new Audit( NAME );
	//private static final Trace trace = new Trace( NAME )
	
	/* Text   : is an html table with the class="infobox vcard"
	 *          it is a 2 column table of th and td tags.
	 *          This class reads and presents it as an Attributes list.
	 *          E.g.:  Section title:     paragraphs
	 *          +-----------------------+----------------------------+
	 *          |                       | <p>Barack Hussein Obama... |
	 *          +-----------------------+----------------------------+
	 *          |                       | <p>                        |
	 *          +-----------------------+----------------------------+
	 *          | <h3> Early Life </h3> | <p>                        |
	 *          +-----------------------+----------------------------+
	 *          |                       | <p>...                            |
	 *          
	 * For testing, see:- get_the-page_from-.entry
	 */
	
	private static final String SEP = "@";

	private final String     fname;
	private final Attributes attrs;
	public        Attributes attrs() {return attrs;}

	private final String source;
	public        String source() {return source;}
	
	private static String decodeSource( String fname ) {
		// "./selftest/wikipedia/topic.html" => "wikipedia"
		String[] source = fname.split( "/" );
		// can't be certain of the length of the path...
		// ...so take if from the end!
		return source[ source.length - 2 ];
	}
	
	private String attribsName = "";
	private String sectionName = "";
	private Topic  topic; // e.g. "Queen Elizabeth The Second"
	
	private Attributes doA( HtmlStream hs, Attributes a ) {
		Attributes ats    = new Attributes();
		audit.in( "doA", "attributes="+ a );
		
		// ======== read the anchor - but ignore!
		Strings value     = hs.getText();
		Html html = hs.getHtml();
		while (!html.isEmpty() &&
				!(html.type() == Html.Type.END &&
				  html.name().equalsIgnoreCase("a")))
		{
			value.addAll( hs.getText());
			html = hs.getHtml();
		}
		
		// ======== ...but do add the relevant details from the a:OPEN
		ats.add(
			new Attribute(
				sectionName +" "+ attribsName,
				a.value( "title" ) +SEP+ a.value( "href" )
		)	);
		
		audit.out( ats );
		return ats;
	}
	private Attributes doLi( HtmlStream hs ) {
		Attributes ats    = new Attributes();
		audit.in( "doLi", "" );
		Strings value     = hs.getText(); // ignoring this ftm
		
		Html html = hs.getHtml();
		while (!html.isEmpty() &&
				!(html.type() == Html.Type.END &&
				  html.name().equalsIgnoreCase("li")))
		{
			if (html.name().equals( "a" )) {
				ats.addAll( doA( hs, html.attributes() ));
				
			} else {
				Strings s = hs.getText();
				value.addAll( s ); // ignoring this ftm
			}
			
			html = hs.getHtml();
		}
		audit.out( ats );
		return ats;
	}
	private String doHeader( HtmlStream hs, String tagName ) {
		audit.in( "doHeader", "" );
		Strings strs = hs.getText();
		Html html = hs.getHtml();
		while (!html.isEmpty()  &&
				!(html.type() == Html.Type.END &&
				  html.name().equals(tagName)))
		{
			strs.addAll( hs.getText() );
			html = hs.getHtml();
		}
		return audit.out( strs.toString() );
	}
	private Attributes doHtml( HtmlStream hs ) {
		Attributes as = new Attributes();
		Html html = hs.getHtml();
		boolean parasfound = false;  // 'Contents' header is before paras
		audit.in( "doHtml", "html="+ html );
		while (!html.isEmpty() &&
				!(parasfound && html.name().equalsIgnoreCase("style")))
		{
			if (html.name().equalsIgnoreCase( "div" ) &&
				html.attributes().contains("class","mw-heading"))
			
				parasfound = true;
				
			else if (parasfound &&
					(html.name().equalsIgnoreCase( "h2" ))&&
							 html.type() != Html.Type.END) {
				
				sectionName = doHeader( hs, html.name() );
				attribsName = "";
			
			} else if (parasfound &&
					(html.name().equalsIgnoreCase( "h3" )) &&
					 html.type() != Html.Type.END) {
			
				attribsName = doHeader( hs, html.name() );
				sectionName = "";
				
				
			} else if (parasfound &&
					html.name().equalsIgnoreCase(  "li" ) &&
					html.type() != Html.Type.END)
			
				as.addAll( doLi( hs ) );
			
			html = hs.getHtml();
		}
		audit.out( as );
		return as;
	}

	private Disambiguation( String fn ) {
		fname  = fn;
		attrs  = doHtml( new HtmlStream( fname ));
		topic  = new Topic(  fname );
		source = decodeSource( fname );
	}
	
	private static String query = "are you referring to";
	private static String query() {return query;}
	public  static void   query( String q ) {query = q;}
	
	private static final String DISAMB_CONCEPT = "disamb";
	
	private Strings ensignSections() {
		
		Strings sections = attrs().uniqOrderedNames();
		for (String section : sections )
			Repertoires.signs().insert(
				new Written( 
					"On \""+ section +"\""
					+ ", list the "+ section  
					+" from the "+ topic +" disambiguation page"
				).toSign().clarification( true ),
				DISAMB_CONCEPT
			);
		
		return Felicity.notOkay(
				"are you looking for "
				+ sections.toString( Config.orListFormat())
			);
	}
	private Strings ensignSection (Strings sectionName) {
		audit.in( "ensignSection", "name="+ sectionName.toString( Strings.DQCSV ));
		Strings sa = new Strings();
			
		for (Attribute a : attrs()) {
			String name = a.name().trim();
			if (sectionName.contains( name )) {
				String[] strs = a.value().split( SEP );
				String cleanlySpoken = strs[0]
						.replace(".", "")
						.replace(",", "" )
						.toLowerCase(); 
				
				sa.append( cleanlySpoken  );
				Repertoires.signs().insert(
					new Written( 
						"On \""+ cleanlySpoken +"\":"
						+ "get the \""+ strs[0] +"\" web page from wikipedia;"
						+ "if not, reply \"sorry, this page at LOCATION was not found\";"
						+ "set page to ... ;"
						+ "get the wikitext for "+ strs[0] +"."
					).toSign().clarification( true ),
					DISAMB_CONCEPT
				);
		}	}
		
		Strings rc = Felicity.notOkay(
						query()
						+ " "
						+ sa.toString( Config.orListFormat())
					);
		
		return audit.out( rc );
	}
	public static boolean isaDisabiguationPage(Attributes as) {
		return as.size() == 1;
	}
	public static Strings disambiguate( String cmd, Strings args, String fname ) {
		Strings rc;
		audit.in( "disambiguate", "cmd="+ cmd +", args="+ args.toString( Strings.DQCSV ) +", fname="+ fname );
		
		Disambiguation d = new Disambiguation( fname );
		
		if (cmd.equals( "get" ))
			rc = d.ensignSections();
		
		else if (cmd.equals( "list" ))
			rc = d.ensignSection( args ); 
		
		else
			rc = Felicity.dnu( cmd );
		
		audit.out( rc );
		return rc;

}	}
