# Machine Understanding

Enguage is a  _Turing Complete_  speech interpreter;
so, you can  _compute ideas_  with words.

Human understanding is, essentially, the  _effective sharing of ideas_.
So, Machine Understanding is taken to be the  _conveyance of ideas to, and their subseqeunt use to interact with, a machine_ .
Ideas can be shared, or created within the machine, by saying: "i can say ...",
and, "where _X_ is a variable";
and then, meaning can be added by saying: "this implies ...".
As a description of language, this is sometimes termed "meta-language".

Before an utterance is made,
and to show this  _conveyance_  has been  _effective_,
these implications can be listed with further meta-language: "what is implied by ..."

[Installation Instructions](doc/install.md)

## Unifying Speech and Computation
As a speech interpeter,
Enguage allows users to execute instructions as speech,
in a similar way to functional programming languages.

For example, defining a [factorial function](etc/rpts/maths/the-of-is-%2Bwhat_is_the-.txt) 
can be done through (meta-)speech by specifying:
"The factorial of 1 is 1.", and,
"The factorial of N is N times the factorial of N minus 1."
Enguage will choose the most appropriate interpretation between these two interpretations.

Computing becomes directly accessible;
the arbitrary transformation of utterances reduces the reliance on written software and,
ultimately, screens and keyboards.

## Machine Understanding vs. AI
Enguage uses speech deterministically whereas AI, particularly LLMs,
generate text in a probablistic manner.
This can be useful in a creative industry,
but Machine Understanding is concerned with the computation,
not regurgitation, of ideas.

Thus, speech forms precise computational instructions, and algortihms become transparent.

This methodology aligns with the principles of the
[Ordinary Language Philosophers](https://en.wikipedia.org/wiki/Ordinary_language_philosophy),
which emphasizes the importance of conveying directly,
without relying on some underlying logic,
such as Montague Semantics,
or the Turing Equivalence between source and machine code.

### Use Enguage

Read [this](doc/contributing.md) to start contributing to Enguage!
See [examples](doc/examples.md).

[Applying this Philosophy](doc/philosophy.md)

[The Prinicples of Enguage](doc/principles.md)

For further examples of repertoires, see in the repo:  __etc/rpts/__  and  __etc/dictionary/__  directory.

The most complete idea is need,needs.txt,
but other examples include meeting.txt which is both a temporal and spatial concept.

Happy talking!

martin@wheatman.net

If you don’t like it, tell me; if you, do tell others!
