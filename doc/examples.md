## Examples
<p>Enguage comes with repertoiress for about 30 pre-built concepts from which you can copy ideas.
   For example, a spatially aware needs list:</p>
<pre/>
        I need milk.
        It is from the dairy aisle.
        I need a coffee.
        And another.
        How many coffees do I need.
        What do I need from the dairy aisle.</pre>
<p>Also a food diary with a default date of today:</op>
<pre>
        I just ate breakfast.
        Today I have eaten a mars bar.
        I have eaten 2 packets of crisps.
        What have I eaten today.
</pre>

- [Programming](doc/programming.md)
- [Examples](doc/examples.md)
- [MySql Example](doc/mySql.md)

## Consumers of Enguage

- [Android](doc/android.md)
- [docker Webserver](doc/container.md)
- [Running the plain jarfile](doc/jarfile.md)
- [Web Server](doc/httpd.md)
- [Shar](doc/shar.md)
- SNAP (WIP)
- [flatpak](doc/flatpak.md)
- [Java Swing](doc/swing.md)

