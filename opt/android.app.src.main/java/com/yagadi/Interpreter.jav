package com.yagadi;

import org.enguage.Enguage;

import java.util.Objects;

public class Interpreter implements Runnable {

    private final MainActivity context;
    private final Strings utterance;

    private final Callback callback;

    public Interpreter(MainActivity m, String u, Callback cb) {
        context = m;
        utterance = new Strings( u );
        callback = cb;
    }

    public void run() {
        String location = Enguage.RW_SPACE;
        try {
            location = Objects.requireNonNull(context.getExternalFilesDir(null)).getPath();
        } catch (NullPointerException ignore) {}

        callback.callback(
            new Enguage( location ).mediateDefaultDebug( utterance )
        );
}   }
